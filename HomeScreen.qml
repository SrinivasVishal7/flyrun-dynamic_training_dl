import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0
import imageWriterBilling 1.0

Rectangle {
    id: root
    width:1920
    height:1080
    focus: true

    property int maxNumberOfItems: 16

    property variant billingItemName
    property int billingQuantity: 1
    property int billingPrice: 0
    property variant  g_traningScreenObject: 0
    property int defaultOrderValue : 1
    property int gridModelListCount : 0
    property bool applicationLaunchedFirstTime: true
    //signal to update the progress bar text
    signal updatePregressBarText(variant progressText,variant progressValue)

    signal quitButtonClicked()
    signal cancelButtonClicked()
    signal factoryRestoreButtonClicked()
    signal traningButtonClicked()
    signal doneButtonClicked()
    signal payButtonClicked()
    signal resetButtonClicked()
    signal homeButtonClicked()
    signal takeSampleButtonClicked()


    Component.onCompleted:
    {
        timeValueTxt.text = Qt.formatDateTime(new Date(), "HH:mm")
        getTotalValue();
        createTraningScreen();
        applicationLaunchedFirstTime = true
        appendGridList()
    }

    function createTraningScreen()
    {
        var traningView = Qt.createComponent( "TraningScreen.qml" );
        if( traningView.status !== Component.Ready )
        {
            if( traningView.status === Component.Error )
                console.log("Error:"+ traningView.errorString() );
            return;
        }
        g_traningScreenObject = traningView.createObject( root, {} );
        g_traningScreenObject.visible = false
        g_traningScreenObject.focus = false
    }

    function resetOrderValue(){
        defaultOrderValue = 1
        order__001.text = "Order # " + defaultOrderValue
    }

    Keys.onPressed:
    {
        if(!g_traningScreenObject.visible)
        {
            keyBoardPressed(event.key,event.modifiers);
    }
    }

    function keyBoardPressed(keyvalue,modifier)
    {
        switch(keyvalue)
        {
        case Qt.Key_Q:
        {
            console.log("**********Quit Key pressed")
            quitButtonClicked();
            Qt.quit()
        }
        break;
        case Qt.Key_C:
        {
            cancelButtonClicked()
            console.log("********** Clear Key pressed")
            clearBillingListModel();
            resetOrderValue();
        }
        break;
        case Qt.Key_T:
        {
            console.log("********** Traning Key pressed")
            console.log("******* Total Trained Item Count",gridModelListCount)
            if(gridModelListCount < maxNumberOfItems)
            {
                productWarningMessage.visible  = false
                traningButtonClicked()
                g_traningScreenObject.visible = true
                g_traningScreenObject.loadTraningScreen()
                g_traningScreenObject.focus = true

            }
            else
            {
                productWarningMessage.visible  = true
                warningMessageTimer.start()
            }
        }
        break;
        case Qt.Key_F:
        {
            if(modifier & Qt.ControlModifier)
            {
                console.log("********** Factory Restore Key pressed")
                applicationLaunchedFirstTime = true
                factoryRestoreButtonClicked()
                clearBillingListModel();
                resetOrderValue();
                productWarningMessage.visible  = false
            }
        }
        break;
        case Qt.Key_P:
        {
            console.log("********** Pay Key pressed")
            payButtonClicked()
            clearBillingListModel();
            updateOrderNumber();
        }
        break;
        /*case Qt.Key_R:
        {
            if (modifier & Qt.ControlModifier)
            {
                console.log("********** Reset Key pressed")
        applicationLaunchedFirstTime = true
                productWarningMessage.visible  = false
                clearBillingListModel();
                resetOrderValue();
                resetGridList()
                resetButtonClicked()
            }
        }
        break;*/
        default:
            break;
        }
    }

    function updateOrderNumber()
    {
        defaultOrderValue = defaultOrderValue + 1
        order__001.text = "Order # " + defaultOrderValue
    }

    function clearBillingListModel()
    {
        billingListModel.clear()
        getTotalValue()
    }


    Timer
    {
        id: datetimeTimer
        interval: (5*1000); running: true; repeat: true;
        onTriggered:
        {
            timeValueTxt.text = Qt.formatDateTime(new Date(), "HH:mm")
        }
    }

    Timer
    {
        id: dateTimer
        interval: (60*60*1000); running: true; repeat: true;
        onTriggered:
        {
            dateValueTxt.text = Qt.formatDateTime(new Date(), "dd/MM/yyyy")
        }
    }

    Timer{
        id:embossTimer
        interval: 500
        repeat: true
        running: false
        onTriggered: {
            productGridView.focus = false
            embossTimer.stop()
        }
    }

    Timer{
        id:warningMessageTimer
        interval: 2000
        repeat: true
        running: false
        onTriggered: {
            productWarningMessage.visible = false
            warningMessageTimer.stop()
        }
    }

    Image {
        id: bg
        source: "images/bg.png"
        x: 0
        y: 0
        opacity: 1
    }

    Image {
        id: orders_tab
        source: "images/orders_tab.png"
        x: 1341
        y: 98
        opacity: 1
    }

    Image {
        id: preview_tab
        source: "images/preview_tab.png"
        x: 0
        y: 0
        opacity: 1
    }

    Image {
        id: tab
        source: "images/tab.png"
        x: 0
        y: -6
        opacity: 1

        Text {
            id: econText1
            text: qsTr("Smart Billing")
            x:0
            y:0
            width: 1920
            height: 100
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 60
            font.family: "Roboto-Bold"
            font.bold: true
            color: "White"
            smooth: true
        }

        Text {
            id: timeValueTxt
            text: "3:53"
            font.pixelSize: 40
            font.family: "Roboto-Bold"
            font.bold: true
            color: "White"
            smooth: true
            x: 1740
            y: 25
            opacity: 1
        }
    }

    Image {
        id: smart_billing_text
        source: "images/smart_billing_text.png"
        x: 108
        y: 5
        opacity: 1
        visible: false
    }

    Image {
        id: econ_logo
        source: "images/econ_logo.png"
        x: 16
        y: 8
        opacity: 1
    }

    Rectangle
    {
        id: productWarningMessage
        x: 500
        y: 500
        width: 700
        height: 100
        color: "yellow"
        border.color: "black"
        border.width: 5
        z: 1
        visible: false
        Text {
            text: qsTr("Maximum Product Limit Reached")
            anchors.centerIn: parent
            font.pixelSize: 30
            font.family: "Roboto-Bold"
            font.bold: true
            color: "red"
            smooth: true
        }
    }

    Item
    {

        Image {
            id: camera_preview
            source: "images/camera_preview.png"
            x: 49
            y: 155
            opacity: 1

            Image
            {
                id: cameraPreview
                anchors.fill: parent

                ImageWriterBilling  {
                    id : imageWriterBilling
                    width : 266
                    height : 266
                }

            }
        }

        Rectangle
        {
            id: cameraPreviewBorder
            x: 40
            y: 145
            width: camera_preview.width + 18
            height: camera_preview.height + 20
            color: "transparent"
            border.color: "red"
            border.width: 5
            radius: 5
            visible: false
        }

        Text {
            id: preview
            text: "PREVIEW"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            x: 8
            y: 109.751602172852
            opacity: 1
        }
        Text {
            id: description
            text: "DESCRIPTION"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            x: 8
            y: 450
            opacity: 1
        }

        Item
        {
            Text {
                id: pay_bill
                text: "Pay Bill"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 515
                opacity: 1
            }

            Text {
                id: back
                text: "Clear"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 585
                opacity: 1
            }
            Text {
                id: training
                text: "Training"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 646
                opacity: 1
            }
            Text {
                id: quit
                text: "Quit"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 713
                opacity: 1
            }
            Text {
                id: done
                text: "Done"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 778
                opacity: 1
            }
            Text {
                id: factory_reset
                text: "Factory Reset"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 843
                opacity: 1
            }
            Text {
                id: factory_reset1
                text: "Factory Reset"
                font.pixelSize: 20
                font.family: "Roboto-Bold"
                font.bold: true
                color: "#ffffff"
                smooth: true
                x: 148
                y: 910
                opacity: 1
                visible: false
            }
            Image {
                id: p_btn
                source: "images/group_3_copy.png"
                x: 35
                y: 500
                opacity: 1

                Text {
                    id: b
                    text: "P"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
            Image {
                id: b_btn
                source: "images/group_3_copy.png"
                x: 35
                y: 564
                opacity: 1
                Text {
                    text: "C"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
            Image {
                id: t_btn
                source: "images/group_3_copy.png"
                x: 35
                y: 630
                opacity: 1
                Text {
                    text: "T"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
            Image {
                id: q_btn
                source: "images/group_3_copy.png"
                x: 35
                y: 697
                opacity: 1
                Text {
                    text: "Q"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
            Image {
                id: d_btn
                source: "images/group_3_copy.png"
                x: 35
                y: 763
                opacity: 1
                Text {
                    text: "D"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
            Image {
                id: r_btn
                source: "images/group_3_copy_2.png"
                x: 35
                y: 829
                opacity: 1
                Text {
                    text: "Ctrl+F"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
            Image {
                id: fnew_btn
                source: "images/group_3_copy_2.png"
                x: 35
                y: 895
                opacity: 1
                visible: false
                Text {
                    text: "Ctrl+F"
                    font.pixelSize: 20
                    font.family: "Roboto-Bold"
                    font.bold: true
                    color: "#ffffff"
                    smooth: true
                    anchors.centerIn: parent
                    opacity: 1
                }
            }
        }
        Image {
            id: heading_grey
            source: "images/heading_grey.png"
            x: 1354
            y: 175
            opacity: 0.25098039215686
        }
    }
    Image {
        id: lines_1
        source: "images/lines_1.png"
        x: 1361
        y: 237
        opacity: 1
    }

    Item
    {
        Text {
            id: order__001
            text: "Order # 1"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#435a9d"
            smooth: true
            x: 1355
            y: 120
            opacity: 1
        }
        Text {
            id: time
            text: "Date:"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#435a9d"
            smooth: true
            x: 1670
            y: 120
            opacity: 1
        }
        Text {
            id: dateValueTxt
            text: Qt.formatDateTime(new Date(), "dd/MM/yyyy")
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#435a9d"
            smooth: true
            x: 1740
            y: 120
            opacity: 1
        }

        Text {
            id: name
            text: "Name"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            color: "#5d5d5d"
            font.bold: true
            smooth: true
            x: 1384
            y: 190
            opacity: 1
        }

        Text {
            id: qty
            text: "Qty"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#5d5d5d"
            smooth: true
            x: 1596
            y: 190
            opacity: 1
        }

        Item
        {
            id: priceList
            x: 1355
            y: 238
            width: 550
            height: 950

            ListModel {
                id: billingListModel
            }

            ScrollView
            {
                width: 550
                height: 600
                ListView {
                    id: itemListView
                    anchors.fill: parent;
                    orientation: ListView.Vertical;
                    model: billingListModel
                    flickableDirection: Flickable.VerticalFlick
                    boundsBehavior: Flickable.StopAtBounds

                    delegate: Rectangle {
                        width: 550;
                        height: 70;
                        //color: index % 2 == 0 ? "light steel blue" : "white"
                        color: index % 2 == 0 ? "light gray" : "white"

                        Text {
                            id: productText
                            height: parent.height;
                            width: 100
                            x: 20
                            y: 20
                            font.pixelSize: 20
                            // delegate can directly use ListElement role name
                            text: itemname
                        }

                        Text {
                            id: quantyText
                            x: 250
                            y: 20
                            height: parent.height;
                            font.pixelSize: 20
                            // delegate can directly use ListElement role name
                            text: quantity
                        }

                        Text {
                            id: pricetext
                            x: 450
                            y: 20
                            height: parent.height;
                            font.pixelSize: 20
                            // delegate can directly use ListElement role name
                            text: "$"+ price
                        }
                    }

                    highlight:Rectangle{
                        width: 300; height: 70
                        radius: 8;
                        opacity: 0.7
                        color: "light blue"
                        focus: true
                        z:1
                        visible: false
                    }
                }
            }
        }


        Text {
            id: total
            text: "Total"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#5d5d5d"
            smooth: true
            x: 1804
            y: 190
            opacity: 1
        }

        Text {
            id: total_
            text: "TOTAL:"
            font.pixelSize: 30
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1382
            y: 950
            opacity: 1
        }
        Text {
            id: totalValueTxt
            text: "$0.00"
            font.pixelSize: 30
            font.family: "Roboto-Regular"
            color: "#000000"
            smooth: true
            x: 1760
            y: 950
            opacity: 1
        }
        Text {
            id: tax
            text: "Tax:"
            font.pixelSize: 30
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1382
            y: 900
            opacity: 1
        }
        Text {
            id: taxValueTxt
            text: "$0.00"
            font.pixelSize: 30
            font.family: "Roboto-Regular"
            color: "#dc283d"
            smooth: true
            x: 1760
            y: 900
            opacity: 1
        }

        Image {
            id: pay_btn
            source: "images/pay_btn.png"
            x: 1690
            y: 1006
            opacity: 1
        }

    }

    Text {
        id: products
        text: "PRODUCTS"
        font.pixelSize: 20
        font.family: "Roboto-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: 383
        y: 103.751602172852
        opacity: 1
    }

    Item
    {
        Item {
            id: displayPage
            x: 370
            y: 150
            width: 1000
            height: 1000

            ListModel {
                id: productGridModel
            }

            ScrollView
            {
                id: itemScrollView
                x:  0
                y:  0
                width: displayPage.width
                height: displayPage.height

                GridView {
                    id: productGridView
                    anchors.fill: parent
                    cellWidth: 250
                    cellHeight: 230
                    focus: true
                    model: productGridModel

                    //highlight: Rectangle { width: 80; height: 80; color: "lightsteelblue" }

                    delegate: Item {
                        id: gridRectangleId
                        width: 200; height: 200

                        Image {
                            id: myIconBg
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            source: imageBg

                            Rectangle {
                                id: rightBackgroundColor
                                //visible: false
                                anchors.fill: parent
                                focus: true
                                border.color: "transparent"
                                border.width: 5
                                color: "transparent"
                                Image {
                                    id: focusedImage
                                    anchors.centerIn: parent
                                    source: gridRectangleId.activeFocus ? "images/btn_selected.png" :imageBg
                                    //source: imageBg
                                    //width: gridRectangleId.activeFocus ? 200: 146
                                    //height: gridRectangleId.activeFocus ? 200: 146

                                    SequentialAnimation {
                                        id: gridAnimation
                                        running: gridRectangleId.activeFocus ? true : false
                                        loops: 1 //Animation.Infinite
                                        NumberAnimation {target: focusedImage;duration: 200; properties: "height,width"; easing.type: Easing.InOutQuad; from:146; to: 200 }
                                        NumberAnimation {target: focusedImage;duration: 200; properties: "height,width"; easing.type: Easing.InOutQuad; from:200; to: 146 }
                                    }
                                }
                            }
                        }

                        Image {
                            id: myIcon
                            x: 37
                            y: 35
                            source: imageIcon
                        }

                        Image
                        {
                            id: newRibbonImage
                            x: 120
                            y: 35
                            source: newItem
                        }


                        Text {
                            x: 35
                            y: 100
                            font.pixelSize: 18
                            font.family: "Roboto-Bold"
                            color: "white"
                            font.bold: true
                            width: 127
                            height: 90
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                            text: productName
                        }
                    }
                }
            }
        }
    }
    Image {
        id: page_scroll
        source: "images/page_scroll.png"
        x: 836
        y: 1039
        opacity: 1
        visible: false
    }

    function appendGridList()
    {
        productGridModel.clear()
        productGridModel.append({"productId":0,"productName":"orange","imageIcon":"images/noodles_img.png","itemPrice":100,"imageBg":"images/grid_btn.png","newItem" : ""})
        productGridModel.append({"productId":1,"productName":"apple","imageIcon":"images/cookies_img.png","itemPrice":10,"imageBg":"images/grid_btn.png","newItem" : ""})
        productGridModel.append({"productId":2,"productName":"pineapple","imageIcon":"images/toys_img.png","itemPrice":50,"imageBg":"images/grid_btn.png","newItem" : ""})
        productGridModel.append({"productId":3,"productName":"biscut","imageIcon":"images/chocolates_img.png","itemPrice":200,"imageBg":"images/grid_btn.png","newItem" : ""})
        productGridModel.append({"productId":4,"productName":"milk","imageIcon":"images/soaps_img.png","itemPrice":20,"imageBg":"images/grid_btn.png","newItem" : ""})

        if(!applicationLaunchedFirstTime)
        {
            applicationLaunchedFirstTime = false
            var index = productGridModel.count - 1
            productGridModel.set(index,{"newItem" : "images/new_icon.png"})
        }

        gridModelListCount = productGridModel.count

        var addItem = maxNumberOfItems - productGridModel.count
        for(var i = productGridModel.count;i < maxNumberOfItems;i++)
        {
            var id = "ID_" + i;
            productGridModel.append({"productName":"","itemPrice":0,"imageIcon":"images/add_item.png","imageBg":"","newItem" : ""})
        }

        applicationLaunchedFirstTime = false
    }

    function populateList( list )
    {
        console.log("I am list : ", list.length)
        productGridModel.clear()

        list.forEach(function (entry) {
            productGridModel.append({"productId":entry[0],"productName":entry[1],"itemPrice":entry[2],"imageIcon":entry[3],"imageBg":"images/grid_btn.png","newItem" : ""})
            console.log("Populate List Item",entry[1])
        });

        if(!applicationLaunchedFirstTime)
        {
            applicationLaunchedFirstTime = false
            var index = productGridModel.count - 1
            productGridModel.set(index,{"newItem" : "images/new_icon.png"})
        }

        gridModelListCount = productGridModel.count

        for(var i = list.length ;i < maxNumberOfItems ;i++)
        {
            var id = "ID_" + i;
            productGridModel.append({"productName":"","itemPrice":0,"imageIcon":"images/add_item.png","imageBg":"","newItem" : ""})
        }

        applicationLaunchedFirstTime = false
    }

    function resetGridList()
    {
        productGridModel.clear()
        for(var i = 0 ;i < maxNumberOfItems ;i++)
        {
            productGridModel.append({"productName":"","itemPrice":0,"imageIcon":"images/add_item.png","imageBg":"","newItem" : ""})
        }
        applicationLaunchedFirstTime = false
        gridModelListCount = 0;
    }

    function appendList()
    {
        billingListModel.clear()
        billingListModel.append({"itemname":"orange","quantity":1,"price":100})
        billingListModel.append({"itemname":"apple","quantity":2,"price":200})
        billingListModel.append({"itemname":"pineapple","quantity":3,"price":300})
        billingListModel.append({"itemname":"biscut","quantity":4,"price":100})
        billingListModel.append({"itemname":"milk","quantity":1,"price":200})

        getTotalValue();
    }

    function getTotalValue()
    {
        var totalValue = 0;
        for(var i=0;i<billingListModel.count;i++)
        {
            var total = billingListModel.get(i).price;
            totalValue = Math.round(totalValue + total);
        }
        var tax = totalValue * (8/100);
        taxValueTxt.text = "$" + Math.round(tax) + ".00"
        var finalTotal = Math.round(tax) + totalValue
        totalValueTxt.text = "$"+ finalTotal + ".00";
    }

    function appendBillingList(index)
    {
        for (var i = 0; i < productGridModel.count; i++)
        {
            if (productGridModel.get(i).productId === index)
            {
                billingItemName = productGridModel.get(i).productName
                billingPrice = productGridModel.get(i).itemPrice
                console.log("*************** Get the Item Name from Grid List",billingItemName,i)

                productGridView.currentIndex = i
                productGridView.forceActiveFocus()
                embossTimer.start()
                break;
            }
        }

        var itemIspresent = false;
        var indexValue = 0

        if(billingListModel.count > 0)
        {
            for (var j = 0; j < billingListModel.count; j++)
            {
                var currentItemName = billingListModel.get(j).itemname;
                var compareItenm = billingItemName
                if(currentItemName === compareItenm)
                {
                    itemIspresent = true;
                    indexValue = j;
                    break;
                }
            }

            if (itemIspresent)
            {
                var quantity = billingListModel.get(j).quantity
                billingQuantity = quantity + 1
                console.log("Item already present, then add the price name",billingQuantity)
                billingPrice = billingQuantity * billingPrice
                billingListModel.set(indexValue,{"itemname":billingItemName,"quantity":billingQuantity,"price":billingPrice})
                console.log("Item already present, then add the Quantity",billingQuantity)
            }
            else
            {
                billingListModel.append({"itemname":billingItemName,"quantity":1,"price":billingPrice})
                console.log("*********** Not existe the item,then add it *******************",billingListModel.count,billingItemName)
            }
        }
        else
        {
            billingQuantity = 1
            billingListModel.append({"itemname":billingItemName,"quantity":1,"price":billingPrice})
            console.log("*********** Append list *******************",billingListModel.count,billingListModel.get(0).itemname)
        }
        getTotalValue()
    }

    function setProgressBarText(progressText,progressValue)
    {
        console.log("Progress Bar text",progressText,progressValue)
        updatePregressBarText(progressText,progressValue)
    }

    function navigateToHomeScreen()
    {
        g_traningScreenObject.visible = false
    }

    function setCameraBorderColor(isRed)
    {
        if(isRed)
        {
            cameraPreviewBorder.border.color = "red"
        }
        else
        {
            cameraPreviewBorder.border.color = "#32CD32"
        }
    }
}


