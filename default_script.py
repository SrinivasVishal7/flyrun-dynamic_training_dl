import os


os.system('cp /home/nvidia/smart_cart_billing_demo/default/model/best_model.hdf5 /home/nvidia/smart_cart_billing_demo/new/model/best_model.hdf5')
os.system('cp /home/nvidia/smart_cart_billing_demo/default/dataset/extractedFeatures/extracted_features.hdf5 /home/nvidia/smart_cart_billing_demo/new/dataset/extractedFeatures/extracted_features.hdf5')
os.system('rm -r /home/nvidia/smart_cart_billing_demo/new/dataset/originalImages/PID_*')
os.system('cp /home/nvidia/smart_cart_billing_demo/default/ApplicationState.json /home/nvidia/smart_cart_billing_demo/new/ApplicationState.json')
