from OpenGL import GL
import sys
from PyQt5 import QtCore, QtGui, QtQml, QtQuick
import cv2 # .cv2 as cv2
from multiprocessing import Process,Queue, Value, Manager
import os
import tensorflow as tf

try: 
    from PyQt5.QtCore import pyqtWrapperType
except ImportError:
    from sip import wrappertype as pyqtWrapperType

import numpy as np
import imutils
from imutils import paths
import keras
from e_con.io import HDF5DatasetWriter
from e_con.io import HDF5DatasetGeneratorFineTune
import h5py
from random import shuffle
#from keras.applications import imagenet_utils
from keras.optimizers import Adam
import collections
import keras.preprocessing.image as keraspreprocessingimage


import time
from random import randint

from distutils.dir_util import copy_tree
import json

from ctypes import c_char_p
'''
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
config = tf.ConfigProto(gpu_options=gpu_options)
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)
'''
# ---------------------- Globals --------------------------------- #


global SAMPLE_SIGNAL_QUEUE
global MESSAGE_QUEUE
global LABEL_QUEUE


global TRAINING_STATE 
global WIDTH
global HEIGHT
global THRESHOLD
global ISDEBUG
global MODEL_PATH
global MAX_QUEUE_SIZE 
global MAXLEN
global KERNEL
global IMAGES_PATH
global OBJECT_IMAGE_PATH
global PREDICTION_QUEUE
global PREVIOUS_PID
global JSON_PATH
global EXTRACTED_FEATURES_PATH_NEW
global EXTRACTED_FEATURES_PATH


ISDEBUG = True
TRAINING_STATE = [False, False]

FRAMES_QUEUE = Queue(1000)
SAMPLE_SIGNAL_QUEUE = Queue(10)
MESSAGE_QUEUE = Queue(10)
# FRAMES_QUEUE_PREDICTION = Queue(1000)
LABEL_QUEUE = Queue(10)
KILL_QUEUE_PREDICTION = Queue(10)
KILL_QUEUE_TRAINING = Queue(10)

WIDTH  = 640
HEIGHT = 480
THRESHOLD = 10
MODEL_PATH = "./new/model/best_model.hdf5"
EXTRACTED_FEATURES_PATH = "./new/dataset/extractedFeatures/extracted_features.hdf5"
EXTRACTED_FEATURES_PATH_NEW = "./new/dataset/extractedFeatures/extracted_features_new.hdf5"
MAX_QUEUE_SIZE = 10
OBJECT_INDEX_COUNTER = [None]
MAXLEN = 50
KERNEL = np.ones((5,5),np.uint8)
IMAGES_PATH = "./new/dataset/originalImages"
OBJECT_IMAGE_PATH = "./images/objects"
JSON_PATH = "./new/ApplicationState.json"
JSON_OBJECT = json.load(open(JSON_PATH))
PREVIOUS_PID = 4
SAMPLES_TAKEN_FLAG = False
json_array = JSON_OBJECT["products"]
last_pid = json_array[-1]["product_id"]
LABELS_STR = last_pid + 2

# --------------------- QT Classes ----------------------------- #

class Singleton(pyqtWrapperType, type):
    def __init__(cls, name, bases, dict):
        super().__init__(name, bases, dict)
        cls.instance=None

    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance=super().__call__(*args, **kw)
        return cls.instance


class ImageWriterBilling(QtQuick.QQuickPaintedItem, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super(ImageWriterBilling, self).__init__(*args, **kwargs)
        self.setRenderTarget(QtQuick.QQuickPaintedItem.FramebufferObject)
        self.cam_frame = QtGui.QImage()

    def paint(self, painter):
        painter.drawImage(0, 0, self.cam_frame)

    def update_frame(self,frame):
        frame = cv2.resize(frame, (266, 266), cv2.INTER_AREA)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        frame = QtGui.QImage(frame, frame.shape[1], frame.shape[0], 17)
        self.cam_frame = frame.copy()
        self.update()

class ImageWriterTraining(QtQuick.QQuickPaintedItem, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super(ImageWriterTraining, self).__init__(*args, **kwargs)
        self.setRenderTarget(QtQuick.QQuickPaintedItem.FramebufferObject)
        self.cam_frame = QtGui.QImage()

    def paint(self, painter):
        painter.drawImage(0, 0, self.cam_frame)

    def update_frame(self,frame):
        frame = cv2.resize(frame, (800, 600), cv2.INTER_CUBIC)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        frame = QtGui.QImage(frame, frame.shape[1], frame.shape[0], 17)
        self.cam_frame = frame.copy()
        self.update()

# ----------------------------------------- Utility Functions -------------------------------------------- #

def fireUpGPU(base_model, classifier_model):
    from keras.applications import imagenet_utils
    dummyImage = np.zeros ((224, 224, 3))
    dummyImage = np.expand_dims(dummyImage, axis=0)
    dummyImage = dummyImage.astype ("float64")
    dummyImage = imagenet_utils.preprocess_input(dummyImage)

    for index in range (3):
        feature = base_model.predict (dummyImage).reshape (1, -1)
        prediction = classifier_model.predict(feature)	

    return True	

def backgroundSubImage (image,bg):
    
    if bg is not None:
        bg = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        return cv2.subtract(image, bg)
    else:
        return None

def detect_contours(img,bg):
    global KERNEL
    global THRESHOLD

    points = []	
    validContours = []
    subImage = backgroundSubImage(img,bg)

    subImage[subImage > THRESHOLD]  = 255
    subImage[subImage <= THRESHOLD] = 0

    subImage = cv2.morphologyEx(subImage, cv2.MORPH_OPEN, KERNEL, iterations = 2)
    subImage = cv2.morphologyEx(subImage, cv2.MORPH_CLOSE, KERNEL, iterations = 10)

    _, contours, _ = cv2.findContours(subImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    IMAGE_SHAPE = subImage.shape

    for c in contours:
        area = cv2.contourArea(c)
        c1, r1, w, h = cv2.boundingRect(c)
        c2 = c1 + w
        r2 = r1 + h
		
        if (c1 > 2 and r1 > 2 and c2 < IMAGE_SHAPE[1]-2 and r2 < IMAGE_SHAPE[0]-2):
            if area > 5000 and area < 100000:
                points.append(cv2.boundingRect(c))
                validContours.append(c)

    return points, validContours

def centerContour ( contourCollection):
    global WIDTH, HEIGHT
    centerIndex = None

    preferedCenter = HEIGHT/2
    minDistance = HEIGHT/2

    for index, contour in enumerate (contourCollection):
        if contour.any() != None:
            c1, r1, w, h = cv2.boundingRect(contour)
            center_row = int (r1 + h/2)
            distance = abs(center_row - preferedCenter)

            if distance < minDistance:
                centerIndex = index
                minDistance = distance
    
    return centerIndex

def adjustObject (image, point,bg):
    global HEIGHT, WIDTH, BACKGROUNDIMAGE

    HEIGHT_TMP = HEIGHT
    WIDTH_TMP  = WIDTH
    c1, r1, w, h = point
    c2 = c1 + w
    r2 = r1 + h

    center_col = (c1+c2)/2
    center_row = (r1+r2)/2

    newImage = bg.copy()

    if (center_row < HEIGHT_TMP/2):
        partitionArea = HEIGHT_TMP - 2*center_row
        
        newImage_row_start = int(partitionArea/2)
        newImage_row_end   = HEIGHT_TMP
        
        origImage_row_start = 0
        origImage_row_end   = HEIGHT_TMP-int(partitionArea/2)
        
    else:
        partitionArea = HEIGHT_TMP - 2*(HEIGHT_TMP - center_row)
        
        newImage_row_start = 0
        newImage_row_end   = HEIGHT_TMP-int(partitionArea/2)
        
        origImage_row_start = int(partitionArea/2)
        origImage_row_end   = HEIGHT_TMP
        
        
    if (center_col < WIDTH_TMP/2):
        partitionArea = WIDTH_TMP - 2*center_col
        
        newImage_col_start = int(partitionArea/2)
        newImage_col_end   = WIDTH_TMP
        
        origImage_col_start = 0
        origImage_col_end   = WIDTH_TMP-int(partitionArea/2)
        
    else:
        partitionArea = WIDTH_TMP - 2*(WIDTH_TMP - center_col)
        
        newImage_col_start = 0
        newImage_col_end   = WIDTH_TMP-int(partitionArea/2)
        
        origImage_col_start = int(partitionArea/2)
        origImage_col_end   = WIDTH_TMP

    tmp = image[origImage_row_start:origImage_row_end, origImage_col_start:origImage_col_end, :]
    newImage = cv2.blur(newImage,(5,5))

    newImage[newImage_row_start:newImage_row_end, newImage_col_start:newImage_col_end, : ] = tmp

    return newImage

def makeItSquareWithBackground (background, image):
	h, w, c = image.shape

	if h>w:
		max_dims = h
	else:
		max_dims = w

	squareImage = np.zeros ((max_dims, max_dims, c), dtype=np.uint8)
	
	if h>w:
		start_index = int((max_dims - w)/2)
		squareImage[:, start_index:start_index+w] = image
		squareImage[:, :start_index] = background[:, :start_index]
		squareImage[:, :start_index+w:] = background[:, w-start_index:]
		
	else:
		start_index = int((max_dims - h)/2)
		squareImage[start_index:start_index+h] = image
		squareImage[:start_index] = background[:start_index]
		squareImage[start_index+h:] = background[h-start_index:]
	
	return squareImage

def centerCrop (frame, required_W, required_H):
	centerOfImage = np.asarray (frame).shape
	
	center_row = int (centerOfImage[0]/2)
	center_col = int (centerOfImage[1]/2)
	
	frame = frame [center_row - int(required_H/2) : center_row + int(required_H/2), center_col - int(required_W/2) : center_col + int(required_W/2) ]
	
	return frame

def populate_list(rootObject, sync_PREVIOUS_PID):
    
    product_prop = []
    product_list = []

    Json_Path = "./new/ApplicationState.json"
    Json_Object = json.load(open(Json_Path))
    json_array = Json_Object['products']

    for item in json_array:
        product_id = item['product_id']
        sync_PREVIOUS_PID.value = product_id
        product_name = item['product_name']
        product_price = item['product_price']
        image_path = item['image_path']
        product_prop = [product_id,product_name,product_price,image_path]
        product_list.append(product_prop)

    rootObject.populateList(product_list)

def rotateImages (images, finalNumberOfImage):
    numberOfImages = len(images)
    rotatedImages = []

    angleDiff = int(int(360*numberOfImages)/finalNumberOfImage)
    print ('angleDiff : {} {} {}'.format(angleDiff, numberOfImages, finalNumberOfImage))

    imageCount = 0
    for image in images:
        for angle in np.arange(0, 360, angleDiff):
            rotated = imutils.rotate(image, angle)
            rotatedImages.append (rotated)
            imageCount += 1
            if imageCount == finalNumberOfImage:
                break
        if imageCount == finalNumberOfImage:
            break
    return rotatedImages

def data_augmentation (images, finalNumberOfImage, batch_size = 2):
    datagen = keraspreprocessingimage.ImageDataGenerator(
        height_shift_range=0.1, 
        shear_range=0.1, zoom_range=0.1,
        )
    
    imageCounter = 0
    augmentedImages = []
    images = np.asarray(images)
    
    for image in datagen.flow(images, batch_size=batch_size):
#       augmentedImages.append(image)
        augmentedImages.extend(image)
        imageCounter = len(augmentedImages)
        if imageCounter == finalNumberOfImage:
            break
    
    augmentedImages = np.asarray (augmentedImages)
    return augmentedImages

def saveImage (images, directoryName, label):
	imageCounter = 0
	for image in images:
		cv2.imwrite (os.path.sep.join([directoryName, label, label+"_"+str(imageCounter) + "_.bmp"]), image)
		imageCounter += 1

def appendHDF5WithVGG16Features (datasetPath, label, numberOfClasses):
    from keras.applications import imagenet_utils
    global EXTRACTED_FEATURES_PATH
    global EXTRACTED_FEATURES_PATH_NEW
    
    from keras.applications import VGG16
    Base_Model = VGG16(weights="imagenet", include_top=False)


    bs = 10

    newImagePath = os.path.sep.join([datasetPath, label])

    imagePaths_temp = list(paths.list_images(datasetPath))
    labels = [p.split(os.path.sep)[-2] for p in imagePaths_temp]
    numberOfClasses += 2
    imagePaths = list(paths.list_images(newImagePath))


    # load the VGG16 network
    print("[INFO] loading network...")
    oldFeaturesPath = EXTRACTED_FEATURES_PATH

    oldFeaturesDB = h5py.File(oldFeaturesPath)
    oldNumImages = oldFeaturesDB["labels"].shape[0]
    labels_map = oldFeaturesDB["label_names"]
    outputDimension = oldFeaturesDB["outputDimension"]

    labels = oldFeaturesDB["labels"]
    labelsList = [l for l in labels]
        
    lateset_labels_map = [lbl for lbl in labels_map]
    lateset_labels_map.append (label)

    totalNumberOfSamples = oldNumImages + len(imagePaths)

    imageIndex = [i for i in range(totalNumberOfSamples)]

    shuffle(imageIndex)

    featuresPath = EXTRACTED_FEATURES_PATH_NEW

    if os.path.isfile(featuresPath):
        os.remove (featuresPath)
    else:
        pass

    datasetWriter = HDF5DatasetWriter((totalNumberOfSamples, 512 * 7 * 7), featuresPath, dataKey="features")
    datasetWriter.storeClassLabels(lateset_labels_map)

    # loop over the images in batches
    print ("totalNumberOfSamples : ", totalNumberOfSamples)
    for Nthbatch in np.arange(0, totalNumberOfSamples, bs):
        #print("Extracting Features\n")

        batchLabels = []
        collectionOfFeatures = []

    #		print ("Nthbatch : ", Nthbatch, imageIndex) 
        for imageID in imageIndex [Nthbatch:Nthbatch+bs]:
            #print ("imageID : ", imageID)
            if imageID < oldNumImages:
                collectionOfFeatures.extend(oldFeaturesDB["features"][imageID].reshape (-1,25088))
                batchLabels.append (oldFeaturesDB["labels"][imageID])
            else:
                image = cv2.imread (imagePaths[imageID - oldNumImages])
                image = np.expand_dims(image, axis=0)
                image = image.astype ("float64")
                image = imagenet_utils.preprocess_input(image)
                print("predicting\n")
                collectionOfFeatures.extend (Base_Model.predict(image).reshape((-1, 512 * 7 * 7)))
                print ("predicted\n")
                batchLabels.append (numberOfClasses-1)

        #print ("adding")
        datasetWriter.add(collectionOfFeatures, batchLabels)
        #print ("added\n")

    if os.path.exists(EXTRACTED_FEATURES_PATH_NEW):
        os.remove(EXTRACTED_FEATURES_PATH)
        os.rename(EXTRACTED_FEATURES_PATH_NEW, EXTRACTED_FEATURES_PATH)     

    del Base_Model
    oldFeaturesDB.close()
    datasetWriter.close()
    return EXTRACTED_FEATURES_PATH, numberOfClasses
# ------------------------------------------- Signal Handlers ---------------------------------------------#

def quit_app():
    print("Killing Application")
    os.system('pkill -9 python')

def trainingButtonClickhandler():
    global TRAINING_STATE
    TRAINING_STATE = [False, True]

def factoryRestoreButtonClickhandler():
    print("hello")

def samplesTakenSignalhandler():
    global SAMPLE_SIGNAL_QUEUE
    SAMPLE_SIGNAL_QUEUE.put(1)

# -------------------------------------------- Processor Functions ---------------------------------------- #

def PredictProcessor(sync_labels_str, FRAMES_QUEUE_PREDICTION):
    from keras.applications import imagenet_utils
    global BACKGROUNDIMAGE
    global MODEL_PATH
    labels_str = sync_labels_str.value

    print ("Prediction labels_str : ", labels_str)
    print ("MODEL_PATH : ", MODEL_PATH)

    for i in range(labels_str):
        OBJECT_INDEX_COUNTER.append(0)

    lastPred = 0
    pred = 0
    lastPred = 0
    lastFrameObject = False

    contourCollection = collections.deque(maxlen=MAXLEN)
    frameCollection   = collections.deque(maxlen=MAXLEN)
    pointsCollection  = collections.deque(maxlen=MAXLEN)

    totalItems = 0
    validFrames = 0

    from keras.models import load_model
    from keras.applications import VGG16
    from keras import backend as K
    import tensorflow as tf
    
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
    config = tf.ConfigProto(gpu_options=gpu_options)
    config.gpu_options.allow_growth=True
    sess = tf.Session(config=config)
    K.set_session(sess)

    Base_Model = VGG16(weights="imagenet", include_top=False)
    classifier_model = load_model(MODEL_PATH)

    print ("Summary\n", classifier_model.summary())

    print ("last layer : ", classifier_model.layers[-1].output)
    if fireUpGPU(Base_Model, classifier_model):
        print ("GPU has been fired\n")

    while True:
        #print ("FRAMES_QUEUE_PREDICTION.empty() : ",FRAMES_QUEUE_PREDICTION.empty())
        if not FRAMES_QUEUE_PREDICTION.empty():
            
            
            Main_frame, Back_ground = FRAMES_QUEUE_PREDICTION.get()
            grayImage = cv2.cvtColor (Main_frame, cv2.COLOR_BGR2GRAY)
            points, validContours = detect_contours(grayImage, Back_ground)
            
            lenPoints = len(points)

            if lenPoints:
                c1, r1, w, h = points[0]
                c2 = c1 + w
                r2 = r1 + h

                if (c1 > 2 and r1 > 2 and c2 < WIDTH-2 and r2 < HEIGHT-2 ):
                    validFrames += 1
                    pointsCollection.append (points[0])
                    contourCollection.append (validContours[0])
                    frameCollection.append (Main_frame.copy())
                    lastFrameObject = True
            else:
                validFrames = 0

                if lastFrameObject:
                    index = centerContour ( contourCollection)
                    frame = adjustObject (frameCollection[index], pointsCollection[index],Back_ground.copy())
                    frame = makeItSquareWithBackground (Back_ground.copy(), frame)
                    frame = centerCrop (frame, 400, 400)
                    
                    frame = cv2.resize (frame, (224, 224))
                    
                    image = np.expand_dims(frame, axis=0)
                    image = image.astype ("float64")
                    image = imagenet_utils.preprocess_input(image)
            
                    feature = Base_Model.predict (image).reshape (1, -1)
                    prediction = classifier_model.predict (feature)
                    
                    pred = np.argmax(prediction, axis=1)[0]
                    lastPred = pred
                    print("------------ Prediction Result ---------------- :", lastPred)
                    if lastPred != 0:
                        LABEL_QUEUE.put(lastPred-1)
            
                    if pred:
                        OBJECT_INDEX_COUNTER[pred] += 1
                        totalItems += 1
                    else:
                        lastFrameObject = False
                                
                lastFrameObject = False
                
                contourCollection = collections.deque(maxlen=MAXLEN)
                frameCollection   = collections.deque(maxlen=MAXLEN)
                pointsCollection  = collections.deque(maxlen=MAXLEN)
    
    # exit()


def TrainingProcessor(sync_labels_str, FRAMES_QUEUE, sync_PREVIOUS_PID):
    
    global MAXLEN
    global MODEL_PATH

    PREVIOUS_PID = sync_PREVIOUS_PID.value
    
    JSON_OBJECT = json.load(open(JSON_PATH))

    datasetPath = IMAGES_PATH
    lastFrameObject = False

    contourCollection = collections.deque(maxlen=MAXLEN)
    frameCollection   = collections.deque(maxlen=MAXLEN)
    pointsCollection  = collections.deque(maxlen=MAXLEN)

    totalItems = 0
    label = None
    originalTrainingSamples = []
    Current_Id = 0

    print("---------------- Training Process Initiated -----------------------")

    ############################################# Sampler Segment #########################################
    
    while True:
        if not FRAMES_QUEUE.empty():

            if not SAMPLE_SIGNAL_QUEUE.empty() :
                val = SAMPLE_SIGNAL_QUEUE.get()
                if val == 1:
                    print("-------------------------- Sampling Over -------------------------")
                    break

            original_image, Back_ground = FRAMES_QUEUE.get()
            frame = original_image.copy()

            grayImage = cv2.cvtColor (frame, cv2.COLOR_BGR2GRAY)
            points, validContours = detect_contours(grayImage,Back_ground)

            lenPoints = len(points)

            if label is None:
                print("------------------ Previous Id --------------------", PREVIOUS_PID)
                Current_Id = PREVIOUS_PID + 1
                label = "PID_" + str(Current_Id)
                sync_labels_str.value += 1
                directoryName = os.path.sep.join([datasetPath, label])
                Json_array = JSON_OBJECT['products']
                Json_array.append(
                    {
                        "product_id" : Current_Id,
                        "product_name" : label,
                        "product_price" : randint(1,50),
                        "image_path" : "./images/objects/New.png"
                    }
                )

                JSON_OBJECT['products'] = Json_array
                
                with open(JSON_PATH, 'w') as data_file:
                    json.dump(JSON_OBJECT, data_file, indent=4)
                
                data_file.close()

                if not os.path.exists(directoryName):
                    os.makedirs(directoryName)

            if lenPoints:
                c1, r1, w, h = points[0]
                c2 = c1 + w
                r2 = r1 + h

                if (c1 > 2 and r1 > 2 and c2 < WIDTH-2 and r2 < HEIGHT-2 ):
                    pointsCollection.append (points[0])
                    contourCollection.append (validContours[0])
                    frameCollection.append (original_image.copy())
                    lastFrameObject = True
            else:
                if lastFrameObject:
                    index = centerContour(contourCollection)
                    originalTrainingSamples.append (frameCollection[index-1])
                    originalTrainingSamples.append (frameCollection[index])
            
                lastFrameObject = False
                
                contourCollection = collections.deque(maxlen=MAXLEN)
                frameCollection   = collections.deque(maxlen=MAXLEN)
                pointsCollection  = collections.deque(maxlen=MAXLEN)
    
    ######################################### Data Augmentation #########################################################

    MESSAGE_QUEUE.put(1)    
    rotatedImages = rotateImages(originalTrainingSamples, 50)
    
    MESSAGE_QUEUE.put(2)

    augmentedPositiveImages_fullImage = data_augmentation(rotatedImages, 50)
    augmentedPositiveImages = []
    
    for frame in augmentedPositiveImages_fullImage:
        frame = centerCrop (frame, 400, 400)
        frame = cv2.resize (frame, (224, 224))
        augmentedPositiveImages.append (frame)
        
    saveImage (augmentedPositiveImages, datasetPath, label)
    
    Batch_Size = 2
    Init_LR = 1e-5
    Num_Epochs = 2

    del rotatedImages, augmentedPositiveImages
    originalTrainingSamples = []
    print (" ----------- Augmentation Done ----------------- ")
    print("--------------- Currentt ID ------------------", Current_Id)
    extractedFeaturesPath, numberOfClasses = appendHDF5WithVGG16Features (datasetPath, label, Current_Id)
    trainGen = HDF5DatasetGeneratorFineTune(extractedFeaturesPath, Batch_Size, classes=numberOfClasses, stringName="features")
    
    labels = trainGen.db["labels"]    
    labelsList = [l for l in labels]
    print ("labelsList : ", labelsList)    
    
    MESSAGE_QUEUE.put(3)

    ########################################### Neural Net Model Training ############################################################
    
    opt = Adam(lr=Init_LR)

    from e_con.nn.conv import tilted_1 as nn_classifier
    from keras import backend as K

    print ('numberOfClasses : ', numberOfClasses)
    model = nn_classifier.build((25088, ), numberOfClasses)
    model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
    
    print (model.summary)
    
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
    config = tf.ConfigProto(gpu_options=gpu_options)
    config.gpu_options.allow_growth=True
    sess = tf.Session(config=config)
    K.set_session(sess)
    
    model.fit_generator(
        trainGen.generator(),
        steps_per_epoch=trainGen.numImages // Batch_Size,
        validation_data=trainGen.generator(),
        validation_steps=trainGen.numImages // Batch_Size,
        epochs=Num_Epochs,
        max_q_size=Batch_Size * 2, verbose=1)
       
    model.save (MODEL_PATH)
    MESSAGE_QUEUE.put(4)	
    del model
    del sess
    del config
    import gc
    gc.collect()
    
    

    #exit()

# ------------------------------------------- Prediction and UI Updation -------------------------------- #

def Predict_Update_Frames(app, imgwb, imgwt,rootObject):
    global ISDEBUG
    global OBJECT_INDEX_COUNTER
    global TRAINING_STATE
    
    BACKGROUNDIMAGE = None
    
    cap = cv2.VideoCapture(1)
    num = 0

    manager = Manager()
    sync_labels_str = manager.Value('i', LABELS_STR)

    

    FRAMES_QUEUE_PREDICTION = Queue (100)
    PredictProcess = Process(target=PredictProcessor, args=[sync_labels_str, FRAMES_QUEUE_PREDICTION])
    PredictProcess.start()

    FRAMES_QUEUE = Queue (100)
    TrainProcess = Process(target=TrainingProcessor, args=[sync_labels_str, FRAMES_QUEUE, sync_PREVIOUS_PID])
    
    while True:
        while num != 30:
            _, BACKGROUNDIMAGE = cap.read()
            num += 1

        _, original_image = cap.read()
        Main_frame = original_image

        if TRAINING_STATE == [True, False]:
            TRAINING_STATE = [False, False]
            print("------------------ LABEL STR ---------------: ",sync_labels_str.value)
            if TrainProcess.is_alive():
                TrainProcess.terminate()
                time.sleep(0.1)
            print("----------- Training Process Killed -----------------------")

            FRAMES_QUEUE_PREDICTION = Queue (100)
            PredictProcess = Process(target=PredictProcessor, args=[sync_labels_str, FRAMES_QUEUE_PREDICTION])
            PredictProcess.start()

        
        if all(TRAINING_STATE) == False:   
            imgwb.update_frame(Main_frame.copy())
            if not FRAMES_QUEUE_PREDICTION.full():
                FRAMES_QUEUE_PREDICTION.put([Main_frame.copy(),BACKGROUNDIMAGE.copy()])

            if not LABEL_QUEUE.empty():
                label = LABEL_QUEUE.get()
                rootObject.appendBillingList(int(label))
            
            
        if TRAINING_STATE == [False, True]:
            TRAINING_STATE = [True, True]
            print("------------------ LABEL STR ---------------: ",sync_labels_str.value)
            if PredictProcess.is_alive():
                PredictProcess.terminate()
                time.sleep(1)
            print("----------- Prediction Process Killed -----------------------")
            FRAMES_QUEUE = Queue (100)
            TrainProcess = Process(target=TrainingProcessor, args=[sync_labels_str, FRAMES_QUEUE, sync_PREVIOUS_PID])
            TrainProcess.start()

        if all(TRAINING_STATE) == True:
            imgwt.update_frame(Main_frame.copy())
            if not FRAMES_QUEUE.full():
                FRAMES_QUEUE.put([Main_frame.copy(),BACKGROUNDIMAGE.copy()])


            if not MESSAGE_QUEUE.empty():
                val = MESSAGE_QUEUE.get_nowait()
                if val == 1:
                    text_on_top = 'Augmenting Samples..'
                    rootObject.setProgressBarText(text_on_top)
                elif val == 2:
                    text_on_top = 'Extracting Features..'
                    rootObject.setProgressBarText(text_on_top)
                elif val == 3:
                    text_on_top = 'Training Neural Net..'
                    rootObject.setProgressBarText(text_on_top)
                elif val == 4:
                    text_on_top = 'Training Completed'
                    rootObject.setProgressBarText(text_on_top)
                    populate_list(rootObject, sync_PREVIOUS_PID)
                    rootObject.navigateToHomeScreen()
                    TRAINING_STATE = [True, False]
                
                
        app.processEvents()

    
if __name__ == "__main__":

    app = QtGui.QGuiApplication(sys.argv)

    QtQml.qmlRegisterType(ImageWriterBilling, "imageWriterBilling", 1, 0, "ImageWriterBilling")
    QtQml.qmlRegisterType(ImageWriterTraining, "imageWriterTraining", 1, 0, "ImageWriterTraining")

    view = QtQuick.QQuickView()
    view.setSource(QtCore.QUrl('HomeScreen.qml'))
    rootObject = view.rootObject()
    view.showFullScreen()
    
    manager_1 = Manager()

    sync_PREVIOUS_PID = manager_1.Value('i', 0)
    populate_list(rootObject, sync_PREVIOUS_PID)

    imgwb = ImageWriterBilling()
    imgwt = ImageWriterTraining()

    

    rootObject.quitButtonClicked.connect(quit_app)
    rootObject.traningButtonClicked.connect(trainingButtonClickhandler)
    # rootObject.homeButtonClicked.connect(homeButtonClickhandler)
    rootObject.factoryRestoreButtonClicked.connect(factoryRestoreButtonClickhandler)
    rootObject.doneButtonClicked.connect(samplesTakenSignalhandler)

    Predict_Update_Frames(app,imgwb,imgwt,rootObject)
    sys.exit(app.exec_())
