import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import imageWriterTraining 1.0


Rectangle {
    id: traningScreenId
    width:1920
    height:1080
    visible: false

    Keys.onPressed:
    {
        keyBoardPressed(event.key);
    }

    function keyBoardPressed(keyvalue)
    {
        switch(keyvalue)
        {
            case Qt.Key_Q:
            {
                Qt.quit()
                root.quitButtonClicked();
                console.log("********** Traning screen Quit Key pressed")
            }
            break;
            case Qt.Key_C:
            {
                root.cancelButtonClicked()
                console.log("********** Traning screen Clear Key pressed")
            }
            break;
            case Qt.Key_D:
            {
                root.doneButtonClicked()
                previewArea.visible = false
                progressBarItem.visible = true
                console.log("********** Traning screen Back Key pressed")
            }
            break;
            case Qt.Key_F:
            {
                console.log("********** Traning screen Factory Restore Key pressed")
                root.factoryRestoreButtonClicked()
            }
            break;
            case Qt.Key_B:
            {
                console.log("********** Traning screen Back Key pressed")
                homeButtonClicked()
                if(progressBarItem.visible)
                {
                    previewArea.visible = true
                    progressBarItem.visible = false
                }
                else
                {
                    root.g_traningScreenObject.visible = false
                }
            }
            break;
            default:
                break;
        }
    }

    Image {
        id: bg
        source: "images/bg.png"
        x: 0
        y: 0
        opacity: 1
    }

    Image {
        id: tab
        source: "images/tab.png"
        x: 0
        y: -20
        opacity: 1
    }

    Image {
        id: tab_logo
        source: "images/tab_logo.png"
        x: 44
        y: 5
        opacity: 1
    }

    Item
    {
        id: previewArea
        x: 560
        y: 240
        width: 800
        height: 600
        Rectangle
        {
            anchors.fill: parent
            border.color: "red"
            border.width: 5
            color: "transparent"
            anchors.centerIn: parent

            ImageWriterTraining  {
                id : imageWriterTraining
                width : 1400
                height : 900
            }

        }
    }

    Item
    {
        id: progressBarItem
        anchors.fill: parent
        visible: false
        ProgressBar
        {
            id: control
            value: 5
            width: 1500
            anchors.centerIn: parent
            height: 50
            clip: true
            minimumValue: 0
            maximumValue: 100
            indeterminate: true
            style: ProgressBarStyle
            {
                background:  Rectangle
                {
                    implicitWidth: 200
                    implicitHeight: 6
                    border.color: "#999999"
                    radius: 5
                }

                progress: Rectangle
                {
                    id: bar
                    width: control.value * parent.width
                    height: parent.height
                    radius: 5

                    LinearGradient
                    {
                        anchors.fill: bar
                        start: Qt.point(0, 0)
                        end: Qt.point(bar.width, 0)
                        source: bar
                        gradient: Gradient
                        {
                            GradientStop { position: 0.0; color: "#17a81a" }
                            GradientStop { id: grad; position: 0.5; color: Qt.lighter("#17a81a", 2) }
                            GradientStop { position: 1.0; color: "#17a81a" }
                        }
                        PropertyAnimation
                        {
                            target: grad
                            property: "position"
                            from: 0.1
                            to: 0.9
                            duration: 1000
                            running: true
                            loops: Animation.Infinite
                        }
                    }
                    LinearGradient
                    {
                        anchors.fill: bar
                        start: Qt.point(0, 0)
                        end: Qt.point(0, bar.height)
                        source: bar
                        gradient: Gradient {
                            GradientStop { position: 0.0; color: Qt.rgba(0,0,0,0) }
                            GradientStop { position: 0.5; color: Qt.rgba(1,1,1,0.3) }
                            GradientStop { position: 1.0; color: Qt.rgba(0,0,0,0.05) }
                        }
                    }
                }
                PropertyAnimation {
                    target: control
                    property: "value"
                    from: 0
                    to: 1
                    duration: 5000
                    running: true
                    loops: Animation.Infinite
                }
            }
        }

        Text
        {
            id: progressText
            text: "Traning in progress..."
            x: progressBarItem.x
            y: (1080/2) + 50
            width: 1920
            height: 100
            font.pixelSize: 30
            font.family: "Roboto-Regular"
            font.bold: true
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            smooth: true
            opacity: 1
        }
    }
}

