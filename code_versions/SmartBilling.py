from OpenGL import GL
import sys
from PyQt5 import QtCore, QtGui, QtQml, QtQuick
import cv2
from threading import Thread
import os
try: from PyQt5.QtCore import pyqtWrapperType
except ImportError:
    from sip import wrappertype as pyqtWrapperType

class Singleton(pyqtWrapperType, type):
    def __init__(cls, name, bases, dict):
        super().__init__(name, bases, dict)
        cls.instance=None

    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance=super().__call__(*args, **kw)
        return cls.instance


class ImageWriterBilling(QtQuick.QQuickPaintedItem, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super(ImageWriterBilling, self).__init__(*args, **kwargs)
        self.setRenderTarget(QtQuick.QQuickPaintedItem.FramebufferObject)
        self.cam_frame = QtGui.QImage()

    def paint(self, painter):
        painter.drawImage(0, 0, self.cam_frame)

    def update_frame(self,frame):
        frame = cv2.resize(frame, (266, 266), cv2.INTER_AREA)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        frame = QtGui.QImage(frame, frame.shape[1], frame.shape[0], 17)
        self.cam_frame = frame.copy()
        self.update()

class ImageWriterTraining(QtQuick.QQuickPaintedItem, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super(ImageWriterTraining, self).__init__(*args, **kwargs)
        self.setRenderTarget(QtQuick.QQuickPaintedItem.FramebufferObject)
        self.cam_frame = QtGui.QImage()

    def paint(self, painter):
        painter.drawImage(0, 0, self.cam_frame)

    def update_frame(self,frame):
        frame = cv2.resize(frame, (800, 600), cv2.INTER_CUBIC)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        frame = QtGui.QImage(frame, frame.shape[1], frame.shape[0], 17)
        self.cam_frame = frame.copy()
        self.update()


def get_frames(app, imgwb, imgwt):
    cap = cv2.VideoCapture(0)
        
    while True:
        while num != 30:
            _ , bgframe = cap.read()
            num += 1
        ret, frame = cap.read()
        if ret:
            imgwb.update_frame(frame.copy())
            imgwt.update_frame(frame.copy())
            app.processEvents()

def populate_list(rootObject):
    list_of_list = [[0,"Orange",100,"images/cookies_img.png"],[1,"Apple",80,"images/noodles_img.png"],[2,"Maggi",50,"images/chocolates_img.png"]]
    rootObject.populateList(list_of_list)

def quit_app():
    print("---------eeeeeeeeeennnnnnnnnnnndddddddddddddddddddd-----------")
    os.system('pkill -9 python')


app = QtGui.QGuiApplication(sys.argv)

QtQml.qmlRegisterType(ImageWriterBilling, "imageWriterBilling", 1, 0, "ImageWriterBilling")
QtQml.qmlRegisterType(ImageWriterTraining, "imageWriterTraining", 1, 0, "ImageWriterTraining")

view = QtQuick.QQuickView()
view.setSource(QtCore.QUrl('HomeScreen.qml'))
rootObject = view.rootObject()
view.showFullScreen()

populate_list(rootObject)

imgwb = ImageWriterBilling()
imgwt = ImageWriterTraining()

rootObject.quitButtonClicked.connect(quit_app)

# get_frames_thread = Thread(target=get_frames, args=(app,imgwb,imgwt,))
# get_frames_thread.start()

get_frames(app,imgwb,imgwt)
print("--------------------------HEllO------------------")
sys.exit(app.exec_())
