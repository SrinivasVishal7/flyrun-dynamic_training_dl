import cv2
import numpy as np
import imutils
import os
from imutils import paths
import keras
from e_con.io import HDF5DatasetWriter
from e_con.io import HDF5DatasetGeneratorFineTune
from keras.applications import VGG16
import progressbar
import h5py
from random import shuffle
from keras.applications import imagenet_utils
from keras.models import load_model
from e_con.nn.conv import tilted_1 as classifier
from keras.optimizers import Adam
import collections
import tensorflow as tf
import keras.preprocessing.image as keraspreprocessingimage
from keras import backend as K

print ("tensorflow version : {}". format (tf.__version__))
print ("Keras version      : {}". format (keras.__version__))
print ("opencv version     : {}". format (cv2.__version__))

'''

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
config = tf.ConfigProto(gpu_options=gpu_options)
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)
'''

cam = cv2.VideoCapture(-1)
if not cam.isOpened():
	print ("cam not found")
	exit()
WIDTH  = 640
HEIGHT = 480	
cam.set (cv2.CAP_PROP_FRAME_WIDTH, WIDTH)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)
#cam.set (cv2.CAP_PROP_FPS, 60)	

THRESHOLD = 10
numberOfClasses = 6
takeSamples = False
isTraining = False
isPrediction = True
originalTrainingSamples = []
backgroundImage = None
backgroundImageBGR = None
isDebug = False


start_row = 100
start_col = 100
croppedSize = 400
cropCentre = True

for _ in range(5):
	_, _ = cam.read()

ret, original_image = cam.read()

backgroundImageBGR = original_image
backgroundImage = cv2.cvtColor (original_image, cv2.COLOR_BGR2GRAY)

original_height, original_width, _ = original_image.shape 
print (original_height, original_width)

if cropCentre:
	start_row = int((original_height - croppedSize)/2)
	start_col = int((original_width - croppedSize)/2)
	

trainingSamplesCounter = 0
label = None
finalNumberOfImage = 100
outputPath = "../dataset/originalImages/"
datasetPath = "../dataset/originalImages/"
BATCH_SIZE = 32

'''
old_model_path = 'model/train_all_in_1/whiteBackground_2.hdf5'
latest_model_path = 'model/train_all_in_1/whiteBackground_2.hdf5'
extractedFeaturesPath = "dataset_all_in_one/extractedFeatures/numberOfClasses_2.hdf5"
'''
old_model_path = "../model/bestModels/numberOfClasses_"+str(numberOfClasses)+".hdf5"
latest_model_path = "../model/bestModels/numberOfClasses_"+str(numberOfClasses)+".hdf5"
extractedFeaturesPath = "../dataset/extractedFeatures/numberOfClasses_"+str(numberOfClasses)+".hdf5"


NUM_EPOCHS = 40
INIT_LR = 1e-5
base_model = VGG16(weights="imagenet", include_top=False)


object_index_counter = [None]

for noc in range (numberOfClasses):
	if noc:
		object_index_counter.append (0)

cv2.namedWindow("Bill")

maxLen = 100


#print ("object_index_counter : {0}". format (object_index_counter))

def largestContour ( contourCollection):
#	print ("contourCollection ", contourCollection)
	maxArea = -1
	maxIndex = None
	for index, contour in enumerate (contourCollection):
		area = cv2.contourArea(contour)
		if maxArea < area:
			maxIndex = index
			maxArea = area
	return maxIndex
	
def centerContour ( contourCollection):
#	print ("contourCollection ", contourCollection)
	centerCounter = -1
	centerIndex = None
	
	preferedCenter = HEIGHT/2
	
	minDistance = HEIGHT/2
	
	for index, contour in enumerate (contourCollection):
		if contour.any() != None:
#			print (str(index)+"_"+str(contour))
			c1, r1, w, h = cv2.boundingRect(contour)
			center_col = int (c1 + w/2)
			center_row = int (r1 + h/2)
		
			distance = abs (center_row - preferedCenter)
		
			if distance < minDistance:
				centerIndex = index
				minDistance = distance
	return centerIndex
		
def centerCrop (frame, required_W, required_H):
	centerOfImage = np.asarray (frame).shape
	
	center_row = int (centerOfImage[0]/2)
	center_col = int (centerOfImage[1]/2)
	
#	print ("center_row - int(required_H/2) : center_row + int(required_H/2), center_col - int(required_W/2) : center_col + int(required_W/2) : ", center_row - int(required_H/2), center_row + int(required_H/2), center_col - int(required_W/2), center_col + int(required_W/2) )
	frame = frame [center_row - int(required_H/2) : center_row + int(required_H/2), center_col - int(required_W/2) : center_col + int(required_W/2) ]
	
	return frame
			

def fireUpGPU(base_model, classifier_model):
	dummyImage = np.zeros ((224, 224, 3))
	dummyImage = np.expand_dims(dummyImage, axis=0)
	dummyImage = dummyImage.astype ("float64")
	dummyImage = imagenet_utils.preprocess_input(dummyImage)
	
	for index in range (3):
		feature = base_model.predict (dummyImage).reshape (1, -1)
		prediction = classifier_model.predict (feature)	
	return True		

def makeItSquareWithBackground (background, image):
	h, w, c = image.shape

	if h>w:
		max_dims = h
	else:
		max_dims = w

	squareImage = np.zeros ((max_dims, max_dims, c), dtype=np.uint8)
	
	if h>w:
		start_index = int((max_dims - w)/2)
		squareImage[:, start_index:start_index+w] = image
		squareImage[:, :start_index] = background[:, :start_index]
		squareImage[:, :start_index+w:] = background[:, w-start_index:]
		
	else:
		start_index = int((max_dims - h)/2)
		squareImage[start_index:start_index+h] = image
		squareImage[:start_index] = background[:start_index]
		squareImage[start_index+h:] = background[h-start_index:]
	
	return squareImage	

def backgroundSubImage (image):
	global backgroundImage
	if backgroundImage is not None:
		return cv2.subtract(image, backgroundImage)
	else:
		None

kernel = np.ones((5,5),np.uint8)

def get_center_from_pointrs (points):
	centers = []
	for point in points:
		x = point [0] + int(point[2]/2)
		y = point [1] + int(point[3]/2)
		center.append ((x,y))
	return center


def detect_contours_1(img):
#	print("detect_contours")
	points = []	
	validContours = []
	subImage = backgroundSubImage (img)
#	cv2.imshow ('subImage', subImage)
	subImage[subImage > THRESHOLD]  = 255
	subImage[subImage <= THRESHOLD] = 0
	subImage = cv2.morphologyEx(subImage, cv2.MORPH_OPEN, kernel, iterations = 2)
	
	_, contours, _ = cv2.findContours(subImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	

	for c in contours:
		area = cv2.contourArea(c)
		
		if area > 1000 and area < 100000:
			points.append(cv2.boundingRect(c))
			validContours.append(c)
	return points, validContours
	

def detect_contours(img):

#	print("detect_contours")
	points = []	
	validContours = []
	subImage = backgroundSubImage (img)

	subImage[subImage > THRESHOLD]  = 255
	subImage[subImage <= THRESHOLD] = 0
	
	subImage = cv2.morphologyEx(subImage, cv2.MORPH_OPEN, kernel, iterations = 2)
	subImage = cv2.morphologyEx(subImage, cv2.MORPH_CLOSE, kernel, iterations = 10)
	cv2.imshow ('subImage', subImage)
	
	_, contours, _ = cv2.findContours(subImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	

	for c in contours:
		area = cv2.contourArea(c)
#		print ("area : ",area)
		if area > 5000 and area < 100000:
			points.append(cv2.boundingRect(c))
			validContours.append(c)
	
	return points, validContours
	
	
def adjustObject (image, point):
	HEIGHT_TMP = HEIGHT
	WIDTH_TMP  = WIDTH
	c1, r1, w, h = point
	c2 = c1 + w
	r2 = r1 + h
	
	center_col = (c1+c2)/2
	center_row = (r1+r2)/2
	
	newImage = backgroundImageBGR.copy()
	
	if (center_row < HEIGHT_TMP/2):
		partitionArea = HEIGHT_TMP - 2*center_row
		
		newImage_row_start = int(partitionArea/2)
		newImage_row_end   = HEIGHT_TMP
		
		origImage_row_start = 0
		origImage_row_end   = HEIGHT_TMP-int(partitionArea/2)
		
		
	else:
		partitionArea = HEIGHT_TMP - 2*(HEIGHT_TMP - center_row)
		
		newImage_row_start = 0
		newImage_row_end   = HEIGHT_TMP-int(partitionArea/2)
		
		origImage_row_start = int(partitionArea/2)
		origImage_row_end   = HEIGHT_TMP
		
		
	if (center_col < WIDTH_TMP/2):
		partitionArea = WIDTH_TMP - 2*center_col
		
		newImage_col_start = int(partitionArea/2)
		newImage_col_end   = WIDTH_TMP
		
		origImage_col_start = 0
		origImage_col_end   = WIDTH_TMP-int(partitionArea/2)
		
	else:
		partitionArea = WIDTH_TMP - 2*(WIDTH_TMP - center_col)
		
		newImage_col_start = 0
		newImage_col_end   = WIDTH_TMP-int(partitionArea/2)
		
		origImage_col_start = int(partitionArea/2)
		origImage_col_end   = WIDTH_TMP
	
	tmp = image[origImage_row_start:origImage_row_end, origImage_col_start:origImage_col_end, :]
	newImage = cv2.blur(newImage,(5,5))

	newImage[newImage_row_start:newImage_row_end, newImage_col_start:newImage_col_end, : ] = tmp
	
	return newImage	

def updateCanvas (objectNames, objectCount, totalItems):	
	
	FONT = cv2.FONT_HERSHEY_SIMPLEX
	FONT_SCALE = 1
	(label_width, label_height), baseline = cv2.getTextSize(objectNames[0], FONT, FONT_SCALE, 2)
	
	#canvas = np.zeros (  ((len(objectName)) * 40 + 10, 500, 3), dtype="uint8")
	
	canvas = np.zeros (  ((len(objectNames)  ) * (label_height + baseline ) + 2*baseline, 500, 3), dtype="uint8")
	
	
	
	for index, objectName in enumerate (objectNames):
		if index:
			
			
#			print (label_width, label_height)
			'''
			cv2.putText(canvas, objectName, (5, 20 + 60*(index -1)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
			cv2.putText(canvas, " : " + str(objectCount[index]), (100, 20 + 60*(index -1)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
			'''
			cv2.putText(canvas, objectName, (5,  (baseline + label_height)*index), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
			cv2.putText(canvas, " : " + str(objectCount[index]), (400, (baseline + label_height)*index), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
		
	cv2.putText(canvas, "Total Items", (5,  (baseline + label_height)*len(objectNames)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
	cv2.putText(canvas, " : " + str(totalItems), (400, (baseline + label_height)*len(objectNames)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
			
			
	cv2.imshow ("Bill", canvas)
	

def rotateImages (images, finalNumberOfImage):
    numberOfImages = len(images)
    rotatedImages = []

    angleDiff = int(int(360*numberOfImages)/finalNumberOfImage)
    print ('angleDiff : {} {} {}'.format(angleDiff, numberOfImages, finalNumberOfImage))

    imageCount = 0
    for image in images:
        for angle in np.arange(0, 360, angleDiff):
            rotated = imutils.rotate(image, angle)
            rotatedImages.append (rotated)
#            cv2.imwrite(os.path.sep.join([outputPath, str(imageCount) + "_" + str(angle) + ".bmp"]), rotated)

            imageCount += 1
            if imageCount == finalNumberOfImage:
                break
        if imageCount == finalNumberOfImage:
            break
    return rotatedImages

def data_augmentation (images, finalNumberOfImage, batch_size = 2):
    datagen = keraspreprocessingimage.ImageDataGenerator(
        height_shift_range=0.1, 
        shear_range=0.1, zoom_range=0.1,
        )
    global isDebug	
    imageCounter = 0
    augmentedImages = []
    images = np.asarray(images)
    if isDebug:
        print (images.shape)
        print ("imageCounter...\n")
    for image in datagen.flow(images, batch_size=batch_size):

#        augmentedImages.append(image)
        augmentedImages.extend(image)
        imageCounter = len(augmentedImages)
        if isDebug:
            print (imageCounter)
        if imageCounter == finalNumberOfImage:
            break
    augmentedImages = np.asarray (augmentedImages)
    if isDebug:
        print ("augmentedImages : ", augmentedImages.shape)
    return augmentedImages

def getPathOfNegative (parentDir, exceptPos):
    imagePaths = list(paths.list_images(parentDir))
    negativeImagePaths = []

    for pt in imagePaths:
        if pt.split(os.path.sep)[-2] not in exceptPos:
            negativeImagePaths.append (pt)

    return negativeImagePaths

def getImages (imagePaths):
    imagePaths = list(paths.list_images(parentDir))

    images = []

    for imagePath in imagePaths:
        image = cv2.imread(imagePath)
        if image == None:
            print (im)
            return None
        images.append (image)

    return images

def saveImage (images, directoryName, label):
	imageCounter = 0
	for image in images:
		cv2.imwrite (os.path.sep.join([directoryName, label, label+"_"+str(imageCounter) + "_.bmp"]), image)
		imageCounter += 1

def appendHDF5WithVGG16Features (datasetPath, label, numberOfClasses):
	print("[INFO] loading images...")

	bs = 10
	global extractedFeaturesPath

	newImagePath = os.path.sep.join([datasetPath, label])

	imagePaths_temp = list(paths.list_images(datasetPath))
	labels = [p.split(os.path.sep)[-2] for p in imagePaths_temp]
	
	'''
	numberOfClasses = [str(x) for x in np.unique(labels)]
	numberOfClasses = len (numberOfClasses)
	'''
	
	numberOfClasses += 1
	
	imagePaths = list(paths.list_images(newImagePath))


	# load the VGG16 network
	print("[INFO] loading network...")
	base_model = VGG16(weights="imagenet", include_top=False)
	
	
	oldFeaturesPath = extractedFeaturesPath
	
	path_to_old_extracted_featuers             = oldFeaturesPath.split (os.path.sep)[:-1]
	extractedFileName_with_ext                 = oldFeaturesPath.split (os.path.sep)[-1]
	extractedFileName_without_ext              = extractedFileName_with_ext.split (".")[0]
	extractedFileName_without_ext_without_NOC  = extractedFileName_without_ext.split ("_")[0]
	NOC                                        = extractedFileName_without_ext.split ("_")[-1]
	NOC = int(NOC)
	new_file_name                              = "_".join ( [extractedFileName_without_ext_without_NOC, str(NOC+1)])
	new_file_name_with_ext                     = ".".join ( [new_file_name, "hdf5"])
	
	latest_model_path                          =  os.path.sep.join (str(directory) for directory in path_to_old_extracted_featuers)
	latest_model_path                          =  os.path.sep.join ([ latest_model_path, new_file_name_with_ext])

	#featuresPath    = os.path.sep.join([datasetPath, "extracted_features", "numberOfClasses_"+str((numberOfClasses))+".hdf5"])
	#oldFeaturesPath = os.path.sep.join([datasetPath, "extracted_features", "numberOfClasses_"+str((numberOfClasses)-1)+".hdf5"])
	
	featuresPath = latest_model_path
	extractedFeaturesPath = latest_model_path
	
	oldFeaturesDB = h5py.File(oldFeaturesPath)
	oldNumImages = oldFeaturesDB["labels"].shape[0]
	labels_map = oldFeaturesDB["label_names"]
	outputDimension = oldFeaturesDB["outputDimension"]

	labels = oldFeaturesDB["labels"]
	labelsList = [l for l in labels]
	
	if isDebug:
		print ("labelsList : ", labelsList)
		print ("labels_map : ",labels_map)
		print ("labels_map : ",len (labels_map))
	
	lateset_labels_map = [lbl for lbl in labels_map]
	lateset_labels_map.append (label)
	
	totalNumberOfSamples = oldNumImages + len(imagePaths)

	imageIndex = [i for i in range(totalNumberOfSamples)]
	
	shuffle(imageIndex)
	
	if os.path.isfile(featuresPath):
		if isDebug:
			print ("\t{0} file exists... deleting the file....". format (featuresPath))
		os.remove (featuresPath)
	else:
		if isDebug:
			print ("\tCreating {0} file....". format (featuresPath))
	
	datasetWriter = HDF5DatasetWriter((totalNumberOfSamples, 512 * 7 * 7), featuresPath, dataKey="features")
	
	datasetWriter.storeClassLabels(lateset_labels_map)

	# initialize the progress bar
	widgets = ["\t\tExtracting Features: ", progressbar.Percentage(), " ",progressbar.Bar(), " ", progressbar.ETA()]
	pbar = progressbar.ProgressBar(maxval=len(imagePaths), widgets=widgets).start()

	# loop over the images in patches
	print ("totalNumberOfSamples : ", totalNumberOfSamples)
	for Nthbatch in np.arange(0, totalNumberOfSamples, bs):

		batchLabels = []
		collectionOfFeatures = []

#		print ("Nthbatch : ", Nthbatch, imageIndex) 
		for imageID in imageIndex [Nthbatch:Nthbatch+bs]:
			if imageID < oldNumImages:
				collectionOfFeatures.extend (oldFeaturesDB["features"][imageID].reshape (-1,25088))
				batchLabels.append (oldFeaturesDB["labels"][imageID])
			else:
				image = cv2.imread (imagePaths[imageID - oldNumImages])
				image = np.expand_dims(image, axis=0)
				image = image.astype ("float64")
				image = imagenet_utils.preprocess_input(image)

				#collectionOfFeatures.append (base_model.predict(image).reshape((-1, 512 * 7 * 7)))
				collectionOfFeatures.extend (base_model.predict(image).reshape((-1, 512 * 7 * 7)))
				
#				print ("numberOfClasses : ", numberOfClasses)
				batchLabels.append (numberOfClasses-1)

		# add the features and labels to our HDF5 dataset
		if isDebug:
			print (np.asarray(collectionOfFeatures).shape, np.asarray(batchLabels).shape)
		datasetWriter.add(collectionOfFeatures, batchLabels)

	# close the dataset
	datasetWriter.close()
	pbar.finish()

	return featuresPath, numberOfClasses



while True:
	if takeSamples == True:
		print ("q : quit\ns : capture sample\nb : capture  background\nt : start training\n")
		
		
		lastFrameObject = False
		contourCollection = collections.deque(maxlen=maxLen)
		frameCollection   = collections.deque(maxlen=maxLen)
		totalItems = 0
		label = None
		startTakingSamples = False
		
		while True:
			ret, original_image = cam.read()
			forBackground = original_image.copy()
			frame = original_image.copy()
			
			grayImage = cv2.cvtColor (frame, cv2.COLOR_BGR2GRAY)
			points, validContours = detect_contours(grayImage)
			
			lenPoints = len(points)
			
			if label == None:
				label = input ("Enter the label : ")
				print ("\nthe label is taken as : {0}\n".format(label))

				directoryName = os.path.sep.join([datasetPath, label])
				if not os.path.exists(directoryName):
					os.makedirs(directoryName)
			
			if startTakingSamples:
			
				if lenPoints:
					c1, r1, w, h = points[0]
					c2 = c1 + w
					r2 = r1 + h
					if (c1 > 2 and r1 > 2 and c2 < WIDTH-2 and r2 < HEIGHT-2 ):
						contourCollection.append (validContours[0])
						frameCollection.append (original_image.copy())
						lastFrameObject = True
			
				else:
					if lastFrameObject:
						index = centerContour ( contourCollection)
						originalTrainingSamples.append (frameCollection[index-1])
						originalTrainingSamples.append (frameCollection[index])
						originalTrainingSamples.append (frameCollection[index+1])
					
						cv2.imwrite ("../dataset/dummydataset/sample_"+str(trainingSamplesCounter)+".bmp", frameCollection[index-1])
						trainingSamplesCounter += 1
					
						cv2.imwrite ("../dataset/dummydataset/sample_"+str(trainingSamplesCounter)+".bmp", frameCollection[index])
						trainingSamplesCounter += 1
					
						cv2.imwrite ("../dataset/dummydataset/sample_"+str(trainingSamplesCounter)+".bmp", frameCollection[index+1])
						trainingSamplesCounter += 1
					
				
					lastFrameObject = False
				
				
					contourCollection = collections.deque(maxlen=maxLen)
					frameCollection   = collections.deque(maxlen=maxLen)
				
			
			cv2.imshow ("original_image", original_image)
			
			
			key = cv2.waitKey(1)
			if chr(key & 255) is 'q':
				takeSamples = False
				exit()

			elif chr(key & 255) is 'b':
				backgroundImage = cv2.cvtColor (forBackground, cv2.COLOR_BGR2GRAY)
				cv2.imwrite ("background.bmp", backgroundImage)
				print ("back ground image is taken...\n")

			elif chr(key & 255) is 's':
				if startTakingSamples:
					startTakingSamples = False
				else:
					startTakingSamples = True
				
				print ("startTakingSamples: {}".format (startTakingSamples))

			elif chr(key & 255) is 't':
				print ("sample data is taken....\n")
				takeSamples = False
				isTraining = True
				cv2.destroyWindow("training_sample") 
				cv2.destroyWindow("original_image")
				break
			

	
	if isTraining == True:
		if isDebug:
			print ("\tperforming rotation...")
		rotatedImages = rotateImages(originalTrainingSamples, 100)
		if isDebug:
			print ("done\n")

		if isDebug:
			print ("\tperforming data augmentation...")
		augmentedPositiveImages_fullImage = data_augmentation (rotatedImages, 100)
		if isDebug:
			print ("done\n")
		
		augmentedPositiveImages = []
		for frame in augmentedPositiveImages_fullImage:
			frame = centerCrop (frame, 400, 400)
			frame = cv2.resize (frame, (224, 224))
			augmentedPositiveImages.append (frame)
			

		if isDebug:
			print ("\tsaving  images...")
		saveImage (augmentedPositiveImages, datasetPath, label)
		
		if isDebug:
			print ("done\n")
		
		

		del rotatedImages, augmentedPositiveImages
		originalTrainingSamples = []

		if isDebug:
			print ("\tcreating dataset...\n")
		extractedFeaturesPath, numberOfClasses = appendHDF5WithVGG16Features (datasetPath, label, numberOfClasses)
		if isDebug:
			print ("done\n")
		
			print ("\tnumberOfClasses : ", numberOfClasses)
			print ("\tStarting training...\n")
			print ("\t\tCreating dataset generator...")
		trainGen = HDF5DatasetGeneratorFineTune(extractedFeaturesPath, BATCH_SIZE, classes=numberOfClasses, stringName="features")
	
		if isDebug:
			print ("done\n")
			print ("trainGen.labels_map      : ", trainGen.labels_map)
			print ("trainGen.numberOfClasses : ", trainGen.numberOfClasses)
			
		labels = trainGen.db["labels"]
		
		labelsList = [l for l in labels]
		
		if isDebug:
			print ("labelsList : ", labelsList)
			print ("\nlabels : \n",trainGen.db["labels"] )
		
		################### training the latest model ################
		
		opt = Adam(lr=INIT_LR)
	
		model = classifier.build((25088, ), numberOfClasses)
		model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
		
		model.fit_generator(
			trainGen.generator(),
			steps_per_epoch=trainGen.numImages // BATCH_SIZE,
			validation_data=trainGen.generator(),
			validation_steps=trainGen.numImages // BATCH_SIZE,
			epochs=NUM_EPOCHS,
			max_q_size=BATCH_SIZE * 2, verbose=1)
		
		
		old_model_path = latest_model_path
		path_to_old_model   = old_model_path.split(os.path.sep)[:-1]
		
		
		model_name_with_ext = old_model_path.split(os.path.sep)[-1]
		model_name_without_ext = model_name_with_ext.split(".")[0]
		model_name_without_numberOfClass = model_name_without_ext.split("_")[0]
		
		latest_model_name_with_numberOfClass = '_'.join([model_name_without_numberOfClass, str(numberOfClasses)])
		latest_model_name_with_ext = ".".join ([latest_model_name_with_numberOfClass, "hdf5"])

#		latest_model_path =  os.path.sep.join ([path_to_old_model, latest_model_name_with_ext])

		latest_model_path =  os.path.sep.join (str(directory) for directory in path_to_old_model)
		latest_model_path =  os.path.sep.join ([ latest_model_path, latest_model_name_with_ext])
		if isDebug:
			print ("latest_model_path : ", latest_model_path)
		
		model.save (latest_model_path)
		
		
		
		
		##############################################################
		
		
		isTraining = False
		isPrediction = True
	
	
	if isPrediction == True:
		if isDebug:
			print("[INFO] loading network...")
		classifier_model = load_model(latest_model_path)
		if isDebug:
			print ("extractedFeaturesPath : ", extractedFeaturesPath)
		oldFeaturesDB = h5py.File(extractedFeaturesPath)
		labels_map = oldFeaturesDB["label_names"]
		labels_str = [l for l in labels_map]
		
		if isDebug:
			print (labels_str) 
		
		print ("q : quit\ns : taking sample\n")
		
		cv2.namedWindow("frame")
		cv2.namedWindow("original_image")
		
		lastPred = 0
		pred = 0
		canICount = True
		color = [0, 255, 0]
		lastPred = 0
		lastFrameObject = False
		contourCollection = collections.deque(maxlen=maxLen)
		frameCollection   = collections.deque(maxlen=maxLen)
		pointsCollection  = collections.deque(maxlen=maxLen)
		'''
		for i in range (maxLen):
			contourCollection.append (None)
			frameCollection.append (None)
			pointsCollection.append (None)
		'''
		totalItems = 0
		
		if fireUpGPU(base_model, classifier_model):
			print ("GPU has been fired\n")
		validFrames = 0
		while True:
			ret, original_image = cam.read()
			forBackground = original_image.copy()
			#frame = original_image [start_row:start_row+croppedSize, start_col:start_col+croppedSize]
			frame = original_image
			
			grayImage = cv2.cvtColor (frame, cv2.COLOR_BGR2GRAY)
			points, validContours = detect_contours(grayImage)
			
			lenPoints = len(points)
			frame = cv2.resize (frame, (224, 224))
		
#			print (points)



			if lenPoints:
				c1, r1, w, h = points[0]
				c2 = c1 + w
				r2 = r1 + h
#				print ("r1, r2, c1, c2 : ", r1, r2, c1, c2)

				if (c1 > 2 and r1 > 2 and c2 < WIDTH-2 and r2 < HEIGHT-2 ):
					validFrames += 1
#					print ("validFrames : ", validFrames)
#					print ("points : ", points)
#					print ("validContours : ", validContours)
					pointsCollection.append (points[0])
					contourCollection.append (validContours[0])
					frameCollection.append (original_image.copy())
					lastFrameObject = True
			else:
				validFrames = 0
				if lastFrameObject:
					index = centerContour ( contourCollection)

					frame = adjustObject (frameCollection[index], pointsCollection[index])
					
					cv2.imshow ("adjustObject", frame)
					frame = makeItSquareWithBackground (backgroundImageBGR, frame)
					
					
					frame = centerCrop (frame, 400, 400)
					
					cv2.imshow ("squareimage", frame)
					

					frame = cv2.resize (frame, (224, 224))
					
					image = np.expand_dims(frame, axis=0)
					image = image.astype ("float64")
					image = imagenet_utils.preprocess_input(image)
			
					feature = base_model.predict (image).reshape (1, -1)
					prediction = classifier_model.predict (feature)
			
					
					pred = np.argmax(prediction, axis=1)[0]
					lastPred = pred
			
					if pred:
						cv2.putText(original_image, labels_str[pred], (100, 100), 0, 0.8, (0, 255, 0), 2, cv2.LINE_AA)
						object_index_counter[pred] += 1
						totalItems += 1
					else:
						lastFrameObject = False
				
				
				lastFrameObject = False
				
				
				
				contourCollection = collections.deque(maxlen=maxLen)
				frameCollection   = collections.deque(maxlen=maxLen)
				pointsCollection  = collections.deque(maxlen=maxLen)
				'''
				for i in range (maxLen):
					contourCollection.append (None)
					frameCollection.append (None)
					pointsCollection.append (None)
				'''
				
			if canICount:
				color = [0, 255, 0]
			else:
				color = [0, 0, 255]

			original_image [start_row, start_col: start_col+croppedSize]             = color
			original_image [start_row+croppedSize, start_col:start_col+croppedSize]  = color
			original_image [start_row:start_row+croppedSize, start_col]              = color
			original_image [start_row:start_row+croppedSize, start_col+croppedSize]  = color
			
			
			updateCanvas (labels_str, object_index_counter, totalItems)
			cv2.imshow ("frame", frame )
			cv2.imshow ("original_image", original_image)
			
			
			key = cv2.waitKey(1)
			if chr(key & 255) is 'q':
				takeSamples = False
				cv2.destroyWindow("frame") 
				cv2.destroyWindow("Bill") 
				cv2.destroyWindow("original_image")
				exit()
				
			elif chr(key & 255) is 'b':
				backgroundImageBGR = forBackground
				backgroundImage =  cv2.cvtColor (forBackground, cv2.COLOR_BGR2GRAY)
				
			
			elif chr(key & 255) is 's':
				object_index_counter.append(0)
				takeSamples = True
				isTraining = False
				isPrediction = False
				cv2.destroyWindow("frame") 
				cv2.destroyWindow("original_image")
				del classifier_model
				break
			
			elif chr(key & 255) is 'r':
				totalItems = 0
				for i in range (len(object_index_counter)):
					object_index_counter[i] = 0
				updateCanvas (labels_str, object_index_counter, totalItems)
			
		# train (trainGen)
