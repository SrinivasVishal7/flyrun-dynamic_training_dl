from OpenGL import GL
import sys
from PyQt5 import QtCore, QtGui, QtQml, QtQuick
import cv2.cv2 as cv2
from multiprocessing import Process,Queue
import os
import tensorflow as tf

try: 
    from PyQt5.QtCore import pyqtWrapperType
except ImportError:
    from sip import wrappertype as pyqtWrapperType

import numpy as np
import imutils
from imutils import paths
import keras
from e_con.io import HDF5DatasetWriter
from e_con.io import HDF5DatasetGeneratorFineTune
from keras.applications import VGG16
import h5py
from random import shuffle
from keras.applications import imagenet_utils
from e_con.nn.conv import tilted_1 as classifier
from keras.optimizers import Adam
import collections
import keras.preprocessing.image as keraspreprocessingimage
from keras import backend as K

from keras import Model
from keras.applications.imagenet_utils import preprocess_input
from sklearn.model_selection import train_test_split
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
import time
from random import randint

from distutils.dir_util import copy_tree
import json

# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
# config = tf.ConfigProto(gpu_options=gpu_options)
# config.gpu_options.allow_growth=True
# sess = tf.Session(config=config)
# K.set_session(sess)

# ---------------------- Globals --------------------------------- #

global FRAMES_QUEUE
global TRAINING_MODE 
global WIDTH
global HEIGHT
global THRESHOLD
global ISDEBUG
global MODEL_PATH
global EXTRACTED_FEATURES_PATH
global MAX_QUEUE_SIZE 
global MAXLEN
global BACKGROUNDIMAGE
global KERNEL
global IMAGES_PATH
global OBJECT_IMAGE_PATH
global JSON_PATH

ISDEBUG = True
TRAINING_MODE = False
FRAMES_QUEUE = Queue(1000)
WIDTH  = 640
HEIGHT = 480
THRESHOLD = 10
MODEL_PATH = "/home/ml/PycharmProjects/smart_cart_billing_demo/new/model/best_model.hdf5"
EXTRACTED_FEATURES_PATH = "/home/ml/PycharmProjects/smart_cart_billing_demo/new/dataset/extractedFeatures/extracted_features.hdf5"
MAX_QUEUE_SIZE = 10
BASE_MODEL = VGG16(weights="imagenet", include_top=False)
OBJECT_INDEX_COUNTER = [None]
MAXLEN = 100
BACKGROUNDIMAGE = None
KERNEL = np.ones((5,5),np.uint8)
IMAGES_PATH = "/home/ml/PycharmProjects/smart_cart_billing_demo/new/dataset/originalImages"
OBJECT_IMAGE_PATH = "/home/ml/PycharmProjects/smart_cart_billing_demo/images/objects"
JSON_PATH = "/home/ml/PycharmProjects/smart_cart_billing_demo/new/ApplicationState.json"

# --------------------- QT Classes ----------------------------- #

class Singleton(pyqtWrapperType, type):
    def __init__(cls, name, bases, dict):
        super().__init__(name, bases, dict)
        cls.instance=None

    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance=super().__call__(*args, **kw)
        return cls.instance


class ImageWriterBilling(QtQuick.QQuickPaintedItem, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super(ImageWriterBilling, self).__init__(*args, **kwargs)
        self.setRenderTarget(QtQuick.QQuickPaintedItem.FramebufferObject)
        self.cam_frame = QtGui.QImage()

    def paint(self, painter):
        painter.drawImage(0, 0, self.cam_frame)

    def update_frame(self,frame):
        frame = cv2.resize(frame, (266, 266), cv2.INTER_AREA)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        frame = QtGui.QImage(frame, frame.shape[1], frame.shape[0], 17)
        self.cam_frame = frame.copy()
        self.update()

class ImageWriterTraining(QtQuick.QQuickPaintedItem, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super(ImageWriterTraining, self).__init__(*args, **kwargs)
        self.setRenderTarget(QtQuick.QQuickPaintedItem.FramebufferObject)
        self.cam_frame = QtGui.QImage()

    def paint(self, painter):
        painter.drawImage(0, 0, self.cam_frame)

    def update_frame(self,frame):
        frame = cv2.resize(frame, (800, 600), cv2.INTER_CUBIC)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        frame = QtGui.QImage(frame, frame.shape[1], frame.shape[0], 17)
        self.cam_frame = frame.copy()
        self.update()

# -------------------------------------------- Processor Functions ---------------------------------------- #

def TraningProcess():
    global TRAINING_MODE
    while True:
        # print("Hello")
        # print(TRAINING_MODE)
        if FRAMES_QUEUE.empty():
            frame = FRAMES_QUEUE.get()
            # print(frame)

# ----------------------------------------- Utility Functions -------------------------------------------- #

def fireUpGPU(base_model, classifier_model):
    dummyImage = np.zeros ((224, 224, 3))
    dummyImage = np.expand_dims(dummyImage, axis=0)
    dummyImage = dummyImage.astype ("float64")
    dummyImage = imagenet_utils.preprocess_input(dummyImage)

    for index in range (3):
        feature = base_model.predict (dummyImage).reshape (1, -1)
        prediction = classifier_model.predict(feature)	

    return True	

def backgroundSubImage (image):
    global BACKGROUNDIMAGE
    
    if BACKGROUNDIMAGE is not None:
        return cv2.subtract(image, cv2.cvtColor(BACKGROUNDIMAGE, cv2.COLOR_BGR2GRAY))
    else:
        None

def detect_contours(img):
    global KERNEL

    points = []	
    validContours = []
    subImage = backgroundSubImage (img)

    subImage[subImage > THRESHOLD]  = 255
    subImage[subImage <= THRESHOLD] = 0

    subImage = cv2.morphologyEx(subImage, cv2.MORPH_OPEN, KERNEL, iterations = 2)
    subImage = cv2.morphologyEx(subImage, cv2.MORPH_CLOSE, KERNEL, iterations = 10)
    # cv2.imshow ('subImage', subImage)

    _, contours, _ = cv2.findContours(subImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    IMAGE_SHAPE = subImage.shape

    for c in contours:
        area = cv2.contourArea(c)
        c1, r1, w, h = cv2.boundingRect(c)
        c2 = c1 + w
        r2 = r1 + h
		
        if (c1 > 2 and r1 > 2 and c2 < IMAGE_SHAPE[1]-2 and r2 < IMAGE_SHAPE[0]-2):
            if area > 3000 and area < 100000:
                points.append(cv2.boundingRect(c))
                validContours.append(c)

    return points, validContours

def centerContour ( contourCollection):
    global WIDTH, HEIGHT
    centerIndex = None

    preferedCenter = HEIGHT/2

    minDistance = HEIGHT/2

    for index, contour in enumerate (contourCollection):
        if contour.any() != None:
            c1, r1, w, h = cv2.boundingRect(contour)
            center_row = int (r1 + h/2)
            distance = abs(center_row - preferedCenter)

            if distance < minDistance:
                centerIndex = index
                minDistance = distance
    
    return centerIndex

def adjustObject (image, point):
    global HEIGHT, WIDTH, BACKGROUNDIMAGE

    HEIGHT_TMP = HEIGHT
    WIDTH_TMP  = WIDTH
    c1, r1, w, h = point
    c2 = c1 + w
    r2 = r1 + h

    center_col = (c1+c2)/2
    center_row = (r1+r2)/2

    newImage = BACKGROUNDIMAGE.copy()

    if (center_row < HEIGHT_TMP/2):
        partitionArea = HEIGHT_TMP - 2*center_row
        
        newImage_row_start = int(partitionArea/2)
        newImage_row_end   = HEIGHT_TMP
        
        origImage_row_start = 0
        origImage_row_end   = HEIGHT_TMP-int(partitionArea/2)
        
        
    else:
        partitionArea = HEIGHT_TMP - 2*(HEIGHT_TMP - center_row)
        
        newImage_row_start = 0
        newImage_row_end   = HEIGHT_TMP-int(partitionArea/2)
        
        origImage_row_start = int(partitionArea/2)
        origImage_row_end   = HEIGHT_TMP
        
        
    if (center_col < WIDTH_TMP/2):
        partitionArea = WIDTH_TMP - 2*center_col
        
        newImage_col_start = int(partitionArea/2)
        newImage_col_end   = WIDTH_TMP
        
        origImage_col_start = 0
        origImage_col_end   = WIDTH_TMP-int(partitionArea/2)
        
    else:
        partitionArea = WIDTH_TMP - 2*(WIDTH_TMP - center_col)
        
        newImage_col_start = 0
        newImage_col_end   = WIDTH_TMP-int(partitionArea/2)
        
        origImage_col_start = int(partitionArea/2)
        origImage_col_end   = WIDTH_TMP

    tmp = image[origImage_row_start:origImage_row_end, origImage_col_start:origImage_col_end, :]
    newImage = cv2.blur(newImage,(5,5))

    newImage[newImage_row_start:newImage_row_end, newImage_col_start:newImage_col_end, : ] = tmp

    return newImage

def makeItSquareWithBackground (background, image):
	h, w, c = image.shape

	if h>w:
		max_dims = h
	else:
		max_dims = w

	squareImage = np.zeros ((max_dims, max_dims, c), dtype=np.uint8)
	
	if h>w:
		start_index = int((max_dims - w)/2)
		squareImage[:, start_index:start_index+w] = image
		squareImage[:, :start_index] = background[:, :start_index]
		squareImage[:, :start_index+w:] = background[:, w-start_index:]
		
	else:
		start_index = int((max_dims - h)/2)
		squareImage[start_index:start_index+h] = image
		squareImage[:start_index] = background[:start_index]
		squareImage[start_index+h:] = background[h-start_index:]
	
	return squareImage

def centerCrop (frame, required_W, required_H):
	centerOfImage = np.asarray (frame).shape
	
	center_row = int (centerOfImage[0]/2)
	center_col = int (centerOfImage[1]/2)
	
	frame = frame [center_row - int(required_H/2) : center_row + int(required_H/2), center_col - int(required_W/2) : center_col + int(required_W/2) ]
	
	return frame

# ------------------------------------------- Prediction and UI Updation -------------------------------- #

def Predict_Update_Frames(app, imgwb, imgwt,labels_str,rootObject):
    global ISDEBUG
    global BACKGROUNDIMAGE
    global OBJECT_INDEX_COUNTER

    if ISDEBUG:
        print("[INFO] loading network...")
    
    classifier_model = load_model(MODEL_PATH)
    
    if ISDEBUG:
        print ("extractedFeaturesPath : ", EXTRACTED_FEATURES_PATH)

    if ISDEBUG:
        print (labels_str)
    
    for i in labels_str:
        OBJECT_INDEX_COUNTER.append(0)

    lastPred = 0
    pred = 0
    canICount = True
    color = [0, 255, 0]
    lastPred = 0
    lastFrameObject = False
    contourCollection = collections.deque(maxlen=MAXLEN)
    frameCollection   = collections.deque(maxlen=MAXLEN)
    pointsCollection  = collections.deque(maxlen=MAXLEN)

    totalItems = 0

    if fireUpGPU(BASE_MODEL, classifier_model):
        print ("GPU has been fired\n")

    validFrames = 0

    cap = cv2.VideoCapture(0)
    num = 0
    PrintCounter = 0
    while True:
        while num != 30:
            _, BACKGROUNDIMAGE = cap.read()
            num += 1

        _, original_image = cap.read()
        frame = original_image
        if TRAINING_MODE == False:    
            imgwb.update_frame(frame.copy())
            grayImage = cv2.cvtColor (frame, cv2.COLOR_BGR2GRAY)
            points, validContours = detect_contours(grayImage)
            
            lenPoints = len(points)
            frame = cv2.resize (frame, (224, 224))

            if lenPoints:
                c1, r1, w, h = points[0]
                c2 = c1 + w
                r2 = r1 + h

                if (c1 > 2 and r1 > 2 and c2 < WIDTH-2 and r2 < HEIGHT-2 ):
                    validFrames += 1
                    pointsCollection.append (points[0])
                    contourCollection.append (validContours[0])
                    frameCollection.append (original_image.copy())
                    lastFrameObject = True
            else:
                validFrames = 0

                if lastFrameObject:
                    index = centerContour ( contourCollection)
                    frame = adjustObject (frameCollection[index], pointsCollection[index])

                    frame = makeItSquareWithBackground (BACKGROUNDIMAGE, frame.copy())
                    frame = centerCrop (frame.copy(), 400, 400)
                    
                    frame = cv2.resize (frame, (224, 224))
                    
                    image = np.expand_dims(frame, axis=0)
                    image = image.astype ("float64")
                    image = imagenet_utils.preprocess_input(image)

                    PrintCounter += 1
                    feature = BASE_MODEL.predict (image).reshape (1, -1)
                    prediction = classifier_model.predict (feature)
                    
                    pred = np.argmax(prediction, axis=1)[0]
                    lastPred = pred

                    if lastPred != 0:
                        print(" ---------- Prediction ------------ : ",labels_str[lastPred])
                        rootObject.appendBillingList(int(lastPred-1))
            
                    if pred:
                        OBJECT_INDEX_COUNTER[pred] += 1
                        totalItems += 1
                    else:
                        lastFrameObject = False
                                
                lastFrameObject = False
                
                contourCollection = collections.deque(maxlen=MAXLEN)
                frameCollection   = collections.deque(maxlen=MAXLEN)
                pointsCollection  = collections.deque(maxlen=MAXLEN)
            
        else :
            imgwt.update_frame(frame.copy())
            FRAMES_QUEUE.put(frame)
                
        app.processEvents()

def populate_list(rootObject, labels_str):
    
    product_prop = []
    product_list = []

    input_file = open (JSON_PATH)
    json_array = json.load(input_file)['products']

    for item in json_array:
        product_id = item['product_id']
        product_name = item['product_name']
        product_price = item['product_price']
        image_path = item['image_path']
        product_prop = [product_id,product_name,product_price,image_path]
        product_list.append(product_prop)

    rootObject.populateList(product_list)

def quit_app():
    print("Killing Application")
    os.system('pkill -9 python')

def trainingButtonClickhandler():
    global TRAINING_MODE
    TRAINING_MODE = True
    TrainProcess = Process(target=TraningProcess)
    TrainProcess.start()

def factoryRestoreButtonClickhandler():
    print("hello")

def homeButtonClickhandler():
    global TRAINING_MODE
    TRAINING_MODE = False
    
if __name__ == "__main__":

    app = QtGui.QGuiApplication(sys.argv)

    QtQml.qmlRegisterType(ImageWriterBilling, "imageWriterBilling", 1, 0, "ImageWriterBilling")
    QtQml.qmlRegisterType(ImageWriterTraining, "imageWriterTraining", 1, 0, "ImageWriterTraining")

    view = QtQuick.QQuickView()
    view.setSource(QtCore.QUrl('HomeScreen.qml'))
    rootObject = view.rootObject()
    view.showFullScreen()

    oldFeaturesDB = h5py.File(EXTRACTED_FEATURES_PATH)
    labels_map = oldFeaturesDB["label_names"]
    labels_str = [l for l in labels_map]
    oldFeaturesDB.close()

    populate_list(rootObject,labels_str)

    imgwb = ImageWriterBilling()
    imgwt = ImageWriterTraining()

    rootObject.quitButtonClicked.connect(quit_app)
    rootObject.traningButtonClicked.connect(trainingButtonClickhandler)
    rootObject.homeButtonClicked.connect(homeButtonClickhandler)
    rootObject.factoryRestoreButtonClicked.connect(factoryRestoreButtonClickhandler)

    Predict_Update_Frames(app,imgwb,imgwt,labels_str,rootObject)
    sys.exit(app.exec_())
