import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0
import imageWriterBilling 1.0

Rectangle {
    id: root
    width:1920
    height:1080
    focus: true

    property int maxNumberOfItems: 16

    property variant billingItemName
    property int billingQuantity: 1
    property int billingPrice: 0
    property variant  g_traningScreenObject: 0

    signal quitButtonClicked()
    signal cancelButtonClicked()
    signal factoryRestoreButtonClicked()
    signal traningButtonClicked()
    signal doneButtonClicked()
    signal payButtonClicked()
    signal resetButtonClicked()
    signal homeButtonClicked()

    Component.onCompleted:
    {
        timeValueTxt.text = Qt.formatDateTime(new Date(), "HH:mm")
        getTotalValue();
        createTraningScreen();
    }

    function createTraningScreen()
    {
        var traningView = Qt.createComponent( "TraningScreen.qml" );
        if( traningView.status !== Component.Ready )
        {
            if( traningView.status === Component.Error )
                console.log("Error:"+ traningView.errorString() );
            return;
        }
        g_traningScreenObject = traningView.createObject( root, {} );
        g_traningScreenObject.visible = false
        g_traningScreenObject.focus = false
    }

    Keys.onPressed:
    {
        keyBoardPressed(event.key);
    }

    function keyBoardPressed(keyvalue)
    {
        switch(keyvalue)
        {
        case Qt.Key_Q:
        {
            console.log("**********Quit Key pressed")
            quitButtonClicked();
            Qt.quit()
        }
        break;
        case Qt.Key_C:
        {
            cancelButtonClicked()
            console.log("********** Clear Key pressed")
        }
        break;
        case Qt.Key_T:
        {
            traningButtonClicked()
            g_traningScreenObject.visible = true
            g_traningScreenObject.focus = true
            console.log("********** Traning Key pressed")
        }
        break;
        case Qt.Key_F:
        {
            console.log("********** Factory Restore Key pressed")
            factoryRestoreButtonClicked()
            clearBillingListModel();
        }
        break;
        case Qt.Key_P:
        {
            console.log("********** Pay Key pressed")
            payButtonClicked()
            clearBillingListModel();
        }
        break;
        case Qt.Key_R:
        {
            console.log("********** Pay Key pressed")
            resetButtonClicked()
        }
        break;
        default:
            break;
        }
    }

    function clearBillingListModel()
    {
        billingListModel.clear()
        getTotalValue()
    }


    Timer
    {
        id: datetimeTimer
        interval: 1000; running: true; repeat: true;
        onTriggered:
        {
          timeValueTxt.text = Qt.formatDateTime(new Date(), "HH:mm")
        }
    }

    Timer{
        id:embossTimer
        interval: 2000
        repeat: true
        running: false
        onTriggered: {
            productGridView.currentIndex = productGridModel.count - 1
            productGridView.forceActiveFocus()
            embossTimer.stop()
        }
    }

    Image {
        id: bg
        source: "images/bg.png"
        x: 0
        y: 0
        opacity: 1
    }
    Image {
        id: orders_tab
        source: "images/orders_tab.png"
        x: 1341
        y: 98
        opacity: 1
    }

    Image {
        id: tab
        source: "images/tab.png"
        x: 0
        y: -20
        opacity: 1
    }

    Image {
        id: tab_logo
        source: "images/tab_logo.png"
        x: 44
        y: 5
        opacity: 1
    }

    Item
    {
        Image {
            id: preview_tab
            source: "images/preview_tab.png"
            x: 0
            y: 0
            opacity: 1
        }
        Image {
            id: camera_preview
            source: "images/camera_preview.png"
            x: 49
            y: 141
            opacity: 1

            Image
            {
                id: cameraPreview
                anchors.fill: parent
                
                ImageWriterBilling  {
                    id : imageWriterBilling
                    width : 266
                    height : 266
        }
                
            }
        }

        Image {
            id: contents
            source: "images/contents.png"
            x: 52
            y: 467
            opacity: 1
        }
        Text {
            id: preview
            text: "PREVIEW"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            x: 8
            y: 109.751602172852
            opacity: 1
        }
        Text {
            id: description
            text: "DESCRIPTION"
            font.pixelSize: 20
            font.family: "Roboto-Bold"
            font.bold: true
            color: "#ffffff"
            smooth: true
            x: 8
            y: 432.751602172852
            opacity: 1
        }
        Image {
            id: heading_grey
            source: "images/heading_grey.png"
            x: 1354
            y: 175
            opacity: 0.25098039215686
        }
    }
    Image {
        id: lines_1
        source: "images/lines_1.png"
        x: 1361
        y: 237
        opacity: 1
    }
    Item
    {
        Text {
            id: order__001
            text: "Order #001"
            font.pixelSize: 20
            font.family: "Roboto-Regular"
            color: "#435a9d"
            smooth: true
            x: 1355
            y: 120
            opacity: 1
        }
        Text {
            id: time
            text: "Time"
            font.pixelSize: 20
            font.family: "Roboto-Regular"
            color: "#435a9d"
            smooth: true
            x: 1714
            y: 120
            opacity: 1
        }
        Text {
            id: timeValueTxt
            text: "3:53"
            font.pixelSize: 20
            font.family: "Roboto-Regular"
            color: "#435a9d"
            smooth: true
            x: 1795
            y: 120
            opacity: 1
        }

        Text {
            id: name
            text: "Name"
            font.pixelSize: 20
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1384
            y: 190
            opacity: 1
        }

        Text {
            id: qty
            text: "Qty"
            font.pixelSize: 20
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1596
            y: 190
            opacity: 1
        }

        Item
        {
            id: priceList
            x: 1355
            y: 238
            width: 550
            height: 950

            ListModel {
                id: billingListModel
            }

            ScrollView
            {
                width: 550
                height: 600
                ListView {
                    id: itemListView
                    anchors.fill: parent;
                    orientation: ListView.Vertical;
                    model: billingListModel
                    flickableDirection: Flickable.VerticalFlick
                    boundsBehavior: Flickable.StopAtBounds

                    delegate: Rectangle {
                        width: 550;
                        height: 70;
                        //color: index % 2 == 0 ? "light steel blue" : "white"
                        color: index % 2 == 0 ? "light gray" : "white"

                        Text {
                            id: productText
                            height: parent.height;
                            width: 100
                            x: 20
                            y: 20
                            font.pixelSize: 20
                            // delegate can directly use ListElement role name
                            text: itemname
                        }

                        Text {
                            id: quantyText
                            x: 250
                            y: 20
                            height: parent.height;
                            font.pixelSize: 20
                            // delegate can directly use ListElement role name
                            text: quantity
                        }

                        Text {
                            id: pricetext
                            x: 450
                            y: 20
                            height: parent.height;
                            font.pixelSize: 20
                            // delegate can directly use ListElement role name
                            text: price
                        }
                    }

                    highlight:Rectangle{
                        width: 300; height: 70
                        radius: 8;
                        opacity: 0.7
                        color: "light blue"
                        focus: true
                        z:1
                        visible: false
                    }
                }
            }
        }


        Text {
            id: total
            text: "Total"
            font.pixelSize: 20
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1804
            y: 190
            opacity: 1
        }

        Text {
            id: total_
            text: "TOTAL:"
            font.pixelSize: 30
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1382
            y: 950
            opacity: 1
        }
        Text {
            id: totalValueTxt
            text: "$0.00"
            font.pixelSize: 30
            font.family: "Roboto-Regular"
            color: "#000000"
            smooth: true
            x: 1760
            y: 950
            opacity: 1
        }
        Text {
            id: tax
            text: "Tax"
            font.pixelSize: 30
            font.family: "Roboto-Medium"
            color: "#5d5d5d"
            smooth: true
            x: 1382
            y: 900
            opacity: 1
        }
        Text {
            id: taxValueTxt
            text: "$0.00"
            font.pixelSize: 30
            font.family: "Roboto-Regular"
            color: "#dc283d"
            smooth: true
            x: 1760
            y: 900
            opacity: 1
        }

        Image {
            id: pay_btn
            source: "images/pay_btn.png"
            x: 1690
            y: 1006
            opacity: 1
        }

    }

    Text {
        id: products
        text: "PRODUCTS"
        font.pixelSize: 20
        font.family: "Roboto-Bold"
        font.bold: true
        color: "#ffffff"
        smooth: true
        x: 383
        y: 103.751602172852
        opacity: 1
    }

    Item
    {
        Item {
            id: displayPage
            x: 370
            y: 150
            width: 1000
            height: 1000

            ListModel {
                id: productGridModel
            }

            ScrollView
            {
                id: itemScrollView
                x:  0
                y:  0
                width: displayPage.width
                height: displayPage.height

                GridView {
                    id: productGridView
                    anchors.fill: parent
                    cellWidth: 250
                    cellHeight: 230
                    focus: true
                    model: productGridModel

                    //highlight: Rectangle { width: 80; height: 80; color: "lightsteelblue" }

                    delegate: Item {
                        id: gridRectangleId
                        width: 200; height: 200

                        Image {
                            id: myIconBg
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            source: imageBg

                            Rectangle {
                                id: rightBackgroundColor
                                visible: false
                                anchors.fill: parent
                                focus: true
                                border.color: gridRectangleId.activeFocus ? "red": "transparent"
                                border.width: 5
                                color: "transparent"
                            }
                        }

                        Image {
                            id: myIcon
                            x: 37
                            y: 35
                            source: imageIcon
                        }


                        Text {
                            x: 35
                            y: 100
                            font.pixelSize: 18
                            font.family: "Roboto-Bold"
                            color: "white"
                            font.bold: true
                            width: 127
                            height: 90
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                            text: productName
                        }
                    }
                }
            }
        }
    }
    Image {
        id: page_scroll
        source: "images/page_scroll.png"
        x: 836
        y: 1039
        opacity: 1
    }

    function appendGridList()
    {
        productGridModel.clear()
        productGridModel.append({"productId":0,"productName":"orange","imageIcon":"images/noodles_img.png","itemPrice":100,"imageBg":"images/grid_btn.png"})
        productGridModel.append({"productId":1,"productName":"apple","imageIcon":"images/cookies_img.png","itemPrice":10,"imageBg":"images/grid_btn.png"})
        productGridModel.append({"productId":2,"productName":"pineapple","imageIcon":"images/toys_img.png","itemPrice":50,"imageBg":"images/grid_btn.png"})
        productGridModel.append({"productId":3,"productName":"biscut","imageIcon":"images/chocolates_img.png","itemPrice":200,"imageBg":"images/grid_btn.png"})
        productGridModel.append({"productId":4,"productName":"milk","imageIcon":"images/soaps_img.png","itemPrice":20,"imageBg":"images/grid_btn.png"})

        var addItem = maxNumberOfItems - productGridModel.count
        for(var i = 0;i < addItem;i++)
        {
            var id = "ID_" + i;
            productGridModel.append({"productName":"","itemPrice":0,"imageIcon":"images/add_item.png","imageBg":""})
        }
    }

    function populateList( list )
    {
        console.log("I am list : ", list.length,list)
        productGridModel.clear()

        list.forEach(function (entry) {
           productGridModel.append({"productId":entry[0],"productName":entry[1],"itemPrice":entry[2],"imageIcon":entry[3],"imageBg":"images/grid_btn.png"})
           console.log("Populate List Item",entry[1])
        });

        for(var i = list.length ;i < maxNumberOfItems ;i++)
        {
            var id = "ID_" + i;
            productGridModel.append({"productName":"","itemPrice":0,"imageIcon":"images/add_item.png","imageBg":""})
        }
    }

    function appendList()
    {
        billingListModel.clear()
        billingListModel.append({"itemname":"orange","quantity":1,"price":100})
        billingListModel.append({"itemname":"apple","quantity":2,"price":200})
        billingListModel.append({"itemname":"pineapple","quantity":3,"price":300})
        billingListModel.append({"itemname":"biscut","quantity":4,"price":100})
        billingListModel.append({"itemname":"milk","quantity":1,"price":200})

        getTotalValue();
    }

    function getTotalValue()
    {
        var totalValue = 0;
        for(var i=0;i<billingListModel.count;i++)
        {
            var total = billingListModel.get(i).price;
            totalValue = Math.round(totalValue + total);
        }
        totalValueTxt.text = "$"+ totalValue + ".00";
    }

    function appendBillingList(index)
    {
        console.log("*********** Billing list index ************",index)
        for (var i = 0; i < productGridModel.count; i++)
        {
            if (productGridModel.get(i).productId === index)
            {
                billingItemName = productGridModel.get(i).productName
                billingPrice = productGridModel.get(i).itemPrice
                console.log("*************** Get the Item Name from Grid List",billingItemName,i)

                productGridView.currentIndex = i
                productGridView.forceActiveFocus()
                embossTimer.start()
                break;
            }
        }

        var itemIspresent = false;
        var indexValue = 0

        if(billingListModel.count > 0)
        {
            for (var j = 0; j < billingListModel.count; j++)
            {
                var currentItemName = billingListModel.get(j).itemname;
                var compareItenm = billingItemName
                if(currentItemName === compareItenm)
                {
                    itemIspresent = true;
                    indexValue = j;
                    break;
                }
            }

            if (itemIspresent)
            {
                var quantity = billingListModel.get(j).quantity
                billingQuantity = quantity + 1
                console.log("Item already present, then add the price name",billingQuantity)
                billingPrice = billingQuantity * billingPrice
                billingListModel.set(indexValue,{"itemname":billingItemName,"quantity":billingQuantity,"price":billingPrice})
                console.log("Item already present, then add the Quantity",billingQuantity)
            }
            else
            {
                billingListModel.append({"itemname":billingItemName,"quantity":1,"price":billingPrice})
                console.log("*********** Not existe the item,then add it *******************",billingListModel.count,billingItemName)
            }
        }
        else
        {
            billingQuantity = 1
            billingListModel.append({"itemname":billingItemName,"quantity":1,"price":billingPrice})
            console.log("*********** Append list *******************",billingItemName)
            console.log("*********** Append list *******************",billingListModel.count,billingListModel.get(0).itemname)
        }
        getTotalValue()
    }
}

