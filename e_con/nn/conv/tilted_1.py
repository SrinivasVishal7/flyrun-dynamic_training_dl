# import the necessary packages
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense, Dropout
from keras import backend as K

class tilted_1:
	@staticmethod
	def build(inputShape, classes):
		# initialize the model along with the input shape to be
		model = Sequential()
		
		model.add(Dense(128, activation="relu", input_shape=inputShape))
		model.add(Dropout (0.5))
		model.add(Dense(classes))
		
		model.add(Dense(classes, activation="relu", input_shape=inputShape))
		model.add(Activation("softmax"))

		# return the constructed network architecture
		return model
