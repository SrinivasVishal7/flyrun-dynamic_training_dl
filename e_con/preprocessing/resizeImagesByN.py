# import the necessary packages
import imutils
import cv2

class resizeImagesByN:
	def __init__(self, N, inter=cv2.INTER_AREA):
		# store the target image width, height, and interpolation
		# method used when resizing
		self.N = N
	def preprocess(self, image):
		resized_image = cv2.resize(image, (len(image[0])/self.N, len(image)/self.N), interpolation=cv2.INTER_LINEAR)
		return resized_image
