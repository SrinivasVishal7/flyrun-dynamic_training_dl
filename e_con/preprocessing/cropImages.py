# import the necessary packages
import imutils
import cv2

class cropImages:
	def __init__(self, x, y, width, height, inter=cv2.INTER_AREA):
		# store the target image width, height, and interpolation
		# method used when resizing
		self.x = x
		self.y = y
		self.width = width
		self.height = height
		print  ('cropped with : ', self.x, self.y, self.width, self.height)

	def preprocess(self, image):
#		print  (image.shape)
		cropped_image = image[self.y:self.y + self.height, self.x:self.x + self.width]
		return cropped_image
